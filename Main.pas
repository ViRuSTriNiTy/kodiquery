//
// Kodi Query plugin for LCDHype
//
// $Id$
//

unit Main;

interface

uses
  ScriptEngineInterface;  // for TScriptFunctionImplementation

type
  TData = record
    data : array[0..65535] of byte;
  end;

function GetID(): TData; stdcall;
procedure Init(); stdcall;
procedure CleanUp(); stdcall;

var
  ConnectToKodi: TScriptFunctionImplementation;
  DisconnectFromKodi: TScriptFunctionImplementation;
  GetConnectionInfo: TScriptFunctionImplementation;
  GetActivePlayer: TScriptFunctionImplementation;
  GetPlayerInfo: TScriptFunctionImplementation;
  GetPlaylistInfo: TScriptFunctionImplementation;
  GetGUIInfo: TScriptFunctionImplementation;
  GetPVRInfo: TScriptFunctionImplementation;
  GetTVShowInfo: TScriptFunctionImplementation;
  GetApplicationInfo: TScriptFunctionImplementation;

implementation

uses
  Windows,            // for MAX_PATH
  SysUtils,           // for StringReplace()
  VersionInformation, // for TVersionInformation
  Sockets,            // for TTCPClient
  KodiHelpers,        // for helper functions
  LazUTF8,            // for UTF8...() functions
  Channel,
  TVShow,
  TypInfo;            // for GetEnumName()

var
  gKodiQueryThread: TKodiQueryThread;

const
  coLCDHypeBooleanValue: array[Boolean] of UnicodeString = ('False', 'True');

{$REGION ' Plugin Helper '}

type
  TPluginHelper = class(TObject)
    public
      function ToLCDHypeString(const AValue: UnicodeString): UnicodeString; inline;
      procedure FreeKodiQueryThread(); inline;
      function QueryPlaylist(const APlaylist: TKodiPlaylist;
                             const AParameter: PScriptFunctionImplementationParameter): UnicodeString;
      function QueryGUIProperty(const APropertyName: UnicodeString): UnicodeString;
      function QueryPVRProperty(const APropertyName: UnicodeString): UnicodeString;
      function QueryPVRChannel(const AParameter: PScriptFunctionImplementationParameter): UnicodeString;
      function QueryPVRCurrentProgram(const AParameter: PScriptFunctionImplementationParameter): UnicodeString;
      function QueryTVShowDetail(const AParameter: PScriptFunctionImplementationParameter): UnicodeString;
      function QueryApplicationProperty(const APropertyName: UnicodeString): UnicodeString;
  end;

function TPluginHelper.ToLCDHypeString(const AValue: UnicodeString): UnicodeString;
var
  r: UTF8String;

begin
  r := UTF8Encode(AValue);
  r := '''' + UTF8StringReplace(r, '''', '''''', [rfReplaceAll]) + '''';

  result := UTF8Decode(r);
end;

procedure TPluginHelper.FreeKodiQueryThread();
begin
  if Assigned(gKodiQueryThread) then
  begin
    gKodiQueryThread.Free;
    gKodiQueryThread := nil;
  end;
end;

function TPluginHelper.QueryPlaylist(const APlaylist: TKodiPlaylist;
                                     const AParameter: PScriptFunctionImplementationParameter): UnicodeString;
var
  lInfoKindParameter, lItemIndexParameter, lItemFieldParameter: PScriptFunctionParameter;
  lItemIndex: Integer;
  lPropertyName: UnicodeString;
  lPlaylistItem: TKodiPlayableItem;
  lQueryItem: Boolean;

begin
  result := '';

  if AParameter^.StructureSize >= SizeOf(TScriptFunctionImplementationParameter) then begin
    if AParameter^.ParameterCount > 1 then begin
      // first parameter is player kind, skip, info already passed via method parameter

      lInfoKindParameter := AParameter^.ParameterList;
      Inc(lInfoKindParameter);

      lItemIndex := -1; // -1 to ensure .Item[] of playlist returns nil
      lPropertyName := '';

      lQueryItem := lInfoKindParameter^.Value = 'Item';
      if lQueryItem then begin

        if AParameter^.ParameterCount > 3 then begin
          lItemIndexParameter := AParameter^.ParameterList;
          Inc(lItemIndexParameter, 2);

          lItemIndex := StrToIntDef(lItemIndexParameter^.Value, 0);

          lItemFieldParameter := AParameter^.ParameterList;
          Inc(lItemFieldParameter, 3);

          lPropertyName := lItemFieldParameter^.Value;
        end;

      end;

      if Assigned(gKodiQueryThread) then begin

        APlaylist.BeginAccess();
        try

          if lQueryItem then begin

            lPlaylistItem := APlaylist.Items[lItemIndex];
            if Assigned(lPlaylistItem) then
              result := ToLCDHypeString(lPlaylistItem.GetProperty(lPropertyName));

          end else
          if lInfoKindParameter^.Value = 'Length' then
            result := IntToStr(APlaylist.Count);

        finally
          APlaylist.EndAccess();
        end;

      end else begin

        if lQueryItem then begin

          if lPropertyName = 'Length' then
            result := '0';

        end else
        if lInfoKindParameter^.Value = 'Length' then
          result := '0';

      end;

    end;
  end;
end;

function TPluginHelper.QueryGUIProperty(const APropertyName: UnicodeString): UnicodeString;
begin
  result := '';

  if Assigned(gKodiQueryThread) then begin
    gKodiQueryThread.GUI.Properties.BeginAccess();
    try
      if APropertyName = 'CurrentWindowID' then
        result := IntToStr(gKodiQueryThread.GUI.Properties.CurrentWindowID)
      else
      if APropertyName = 'CurrentWindowLabel' then
        result := ToLCDHypeString(gKodiQueryThread.GUI.Properties.CurrentWindowLabel)
      else
      if APropertyName = 'CurrentControl' then
        result := ToLCDHypeString(gKodiQueryThread.GUI.Properties.CurrentControl);

    finally
      gKodiQueryThread.GUI.Properties.EndAccess();
    end;
  end;
end;

function TPluginHelper.QueryApplicationProperty(const APropertyName: UnicodeString): UnicodeString;
begin
  result := '';

  if Assigned(gKodiQueryThread) then begin
    gKodiQueryThread.Application.Properties.BeginAccess();
    try
      if APropertyName = 'Muted' then
        result := coLCDHypeBooleanValue[gKodiQueryThread.Application.Properties.Muted]
      else
      if APropertyName = 'Volume' then
        result := IntToStr(gKodiQueryThread.Application.Properties.Volume);

    finally
      gKodiQueryThread.Application.Properties.EndAccess();
    end;
  end;
end;

function TPluginHelper.QueryPVRProperty(const APropertyName: UnicodeString): UnicodeString;
begin
  result := '';

  if Assigned(gKodiQueryThread) then begin
    gKodiQueryThread.GUI.Properties.BeginAccess();
    try
      if APropertyName = 'Available' then
        result := coLCDHypeBooleanValue[gKodiQueryThread.PVR.Properties.Available]
      else
      if APropertyName = 'Recording' then
        result := coLCDHypeBooleanValue[gKodiQueryThread.PVR.Properties.Recording]
      else
      if APropertyName = 'Scanning' then
        result := coLCDHypeBooleanValue[gKodiQueryThread.PVR.Properties.Scanning];

    finally
      gKodiQueryThread.GUI.Properties.EndAccess();
    end;
  end;
end;

function TPluginHelper.QueryPVRChannel(const AParameter: PScriptFunctionImplementationParameter): UnicodeString;
var
  lInfoKindParameter, lItemIndexParameter, lItemFieldParameter: PScriptFunctionParameter;
  lItemIndexOrID: Integer;
  lPropertyName: UnicodeString;
  lChannelItem: TKodiPVRChannel;
  lQueryItemByIndex, lQueryItemByID: Boolean;

begin
  result := '';

  if AParameter^.StructureSize >= SizeOf(TScriptFunctionImplementationParameter) then begin
    if AParameter^.ParameterCount > 1 then begin
      // first parameter is info kind, skip it, already processed outside

      lInfoKindParameter := AParameter^.ParameterList;
      Inc(lInfoKindParameter);

      lItemIndexOrID := -1; // -1 to ensure .Item[] or GetChannelByID() returns nil
      lPropertyName := '';

      lQueryItemByIndex := lInfoKindParameter^.Value = 'ByIndex';
      if not lQueryItemByIndex then
        lQueryItemByID := lInfoKindParameter^.Value = 'ByID'
      else
        lQueryItemByID := false;

      if lQueryItemByIndex or lQueryItemByID then begin

        if AParameter^.ParameterCount > 3 then begin
          lItemIndexParameter := AParameter^.ParameterList;
          Inc(lItemIndexParameter, 2);

          lItemIndexOrID := StrToIntDef(lItemIndexParameter^.Value, 0);

          lItemFieldParameter := AParameter^.ParameterList;
          Inc(lItemFieldParameter, 3);

          lPropertyName := lItemFieldParameter^.Value;
        end;

      end;

      if Assigned(gKodiQueryThread) then begin

        gKodiQueryThread.PVR.ChannelList.BeginAccess();
        try

            if lQueryItemByIndex then begin

              lChannelItem := gKodiQueryThread.PVR.ChannelList.Items[lItemIndexOrID];
              if Assigned(lChannelItem) then
                result := ToLCDHypeString(lChannelItem.GetProperty(lPropertyName));

            end else
            if lQueryItemByID then begin

              if gKodiQueryThread.PVR.ChannelList.GetChannelByID(lItemIndexOrID, lChannelItem) then
                result := ToLCDHypeString(lChannelItem.GetProperty(lPropertyName));

            end else
            if lInfoKindParameter^.Value = 'Count' then
              result := IntToStr(gKodiQueryThread.PVR.ChannelList.Count);

        finally
          gKodiQueryThread.PVR.ChannelList.EndAccess();
        end;

      end else begin

        if lQueryItemByIndex or lQueryItemByID then begin

          if lPropertyName = 'Count' then
            result := '0';

        end else
        if lInfoKindParameter^.Value = 'Count' then
          result := '0';

      end;

    end;
  end;
end;

function TPluginHelper.QueryPVRCurrentProgram(const AParameter: PScriptFunctionImplementationParameter): UnicodeString;
var
  lInfoKindParameter: PScriptFunctionParameter;
  lCurrentProgram: TKodiPVRCurrentProgram;
  lDateTime: TDateTime;
  s: AnsiString;

begin
  result := '';

  if AParameter^.StructureSize >= SizeOf(TScriptFunctionImplementationParameter) then begin
    if AParameter^.ParameterCount > 1 then begin
      // first parameter is info kind, skip it, already processed outside

      lInfoKindParameter := AParameter^.ParameterList;
      Inc(lInfoKindParameter);

      if Assigned(gKodiQueryThread) then begin
        lCurrentProgram :=  gKodiQueryThread.PVR.CurrentProgram;

        lCurrentProgram.BeginAccess();
        try

          if lInfoKindParameter^.Value = 'State' then
            result := ToLCDHypeString(coKodiPlayerStateStr[lCurrentProgram.State])
          else
          if lInfoKindParameter^.Value = 'ChannelID' then
            result := IntToStr(lCurrentProgram.ChannelID)
          else
          if lInfoKindParameter^.Value = 'Name' then
            result := ToLCDHypeString(lCurrentProgram.Name)
          else
          if lInfoKindParameter^.Value = 'StartTime' then begin
            if AParameter^.ParameterCount > 2 then begin
              Inc(lInfoKindParameter);

              lDateTime := lCurrentProgram.MsToDateTime(lCurrentProgram.StartTime);

              try
                DateTimeToString(s, lInfoKindParameter^.Value, lDateTime);

                result := ToLCDHypeString(s);
              except
                on e: Exception do
                  // most likely wrong format
              end;
            end;
          end else
          if lInfoKindParameter^.Value = 'EndTime' then begin
            if AParameter^.ParameterCount > 2 then begin
              Inc(lInfoKindParameter);

              lDateTime := lCurrentProgram.MsToDateTime(lCurrentProgram.EndTime);

              try
                DateTimeToString(s, lInfoKindParameter^.Value, lDateTime);

                result := ToLCDHypeString(s);
              except
                on e: Exception do
                  // most likely wrong format
              end;
            end;
          end else
          if lInfoKindParameter^.Value = 'Position' then
            result := IntToStr(lCurrentProgram.Position)
          else
          if lInfoKindParameter^.Value = 'Length' then
            result := IntToStr(lCurrentProgram.Length)
          else
          if lInfoKindParameter^.Value = 'Genre' then
            result := ToLCDHypeString(lCurrentProgram.Genre);

        finally
          lCurrentProgram.EndAccess();
        end;
      end;
    end;
  end;
end;

function TPluginHelper.QueryTVShowDetail(const AParameter: PScriptFunctionImplementationParameter): UnicodeString;
var
  lParameter: PScriptFunctionParameter;
  lTVShowID: Integer;
  lTVShow: TKodiTVShow;
  lPropertyName: UnicodeString;

begin
  result := '';

  if AParameter^.StructureSize >= SizeOf(TScriptFunctionImplementationParameter) then begin
    if AParameter^.ParameterCount > 2 then begin

      // first parameter is info kind, skip it, already processed outside

      lParameter := AParameter^.ParameterList;
      Inc(lParameter);

      if Assigned(gKodiQueryThread) then begin

        gKodiQueryThread.TVShows.BeginAccess();
        try
          lTVShowID := StrToIntDef(lParameter^.Value, 0);

          if gKodiQueryThread.TVShows.GetTVShowByID(lTVShowID, lTVShow) then begin
            Inc(lParameter);

            lPropertyName := lParameter^.Value;

            result := ToLCDHypeString(lTVShow.GetProperty(lPropertyName));
          end;

        finally
          gKodiQueryThread.TVShows.EndAccess();
        end;
      end;
    end;
  end;
end;

var
  gPluginHelper: TPluginHelper;

{$ENDREGION}

{$REGION ' Plugin interface '}

function GetID(): TData; stdcall;
var
  c: integer;
  ids: TData;
  lLibraryLocation, lID: String;
  lVersionInformation: TVersionInformation;

begin
  SetLength(lLibraryLocation, MAX_PATH);
  GetModuleFileName(hInstance, PChar(lLibraryLocation), MAX_PATH);

  lVersionInformation := TVersionInformation.Create();
  try
    lVersionInformation.Retrieve(lLibraryLocation);

    lID := lVersionInformation.FileDescription + ' ' + lVersionInformation.Version;
  finally
    lVersionInformation.Free;
  end;

  // converts ID to TData format
  for c:=0 to length(lID)-1 do
    ids.data[c] := ord(lID[c+1]);
  for c:=length(lID) to sizeof(ids.data)-1 do
    ids.data[c] := 0;

  result := ids;
end;

procedure CleanUp; stdcall;
begin
  gPluginHelper.FreeKodiQueryThread();
end;

procedure Init; stdcall;
begin
  if not Assigned(gKodiQueryThread) then
    gKodiQueryThread := TKodiQueryThread.Create();
end;

{$ENDREGION}

{$REGION ' Script engine interface '}

var
  gReturnValue: WideString = '';

function Library_ConnectToKodi(const AParameter: PScriptFunctionImplementationParameter): PWideChar; stdcall;
var
  lHost, lPort: PScriptFunctionParameter;

begin
  gReturnValue := 'False';

  if AParameter^.StructureSize >= SizeOf(TScriptFunctionImplementationParameter) then begin
    if AParameter^.ParameterCount > 1 then begin
      lHost := AParameter^.ParameterList;

      lPort := AParameter^.ParameterList;
      Inc(lPort);

      if Assigned(gKodiQueryThread) then
      begin
        gKodiQueryThread.Connect(lHost^.Value, StrToIntDef(lPort^.Value, 0));

        // return lcdhype keywords to be able to use function in comparisons directly
        gReturnValue := coLCDHypeBooleanValue[true];
      end;
    end;
  end;

  result := PWideChar(gReturnValue);
end;

function Library_DisconnectFromKodi(const AParameter: PScriptFunctionImplementationParameter): PWideChar; stdcall;
begin
  gReturnValue := '';

  if Assigned(gKodiQueryThread) then
    gKodiQueryThread.Disconnect();

  result := PWideChar(gReturnValue);
end;

function Library_GetConnectionInfo(const AParameter: PScriptFunctionImplementationParameter): PWideChar; stdcall;
var
  lInfoKindParameter: PScriptFunctionParameter;

begin
  gReturnValue := '';

  if AParameter^.StructureSize >= SizeOf(TScriptFunctionImplementationParameter) then begin
    if AParameter^.ParameterCount > 0 then begin
      lInfoKindParameter := AParameter^.ParameterList;

      if Assigned(gKodiQueryThread) then begin

        gKodiQueryThread.Connection.BeginAccess();
        try

          if lInfoKindParameter^.Value = 'Host' then
            gReturnValue := gKodiQueryThread.Connection.Host
          else
          if lInfoKindParameter^.Value = 'Port' then
            gReturnValue := IntToStr(gKodiQueryThread.Connection.Port)
          else
          if lInfoKindParameter^.Value = 'State' then begin
            gReturnValue := GetEnumName(TypeInfo(TKodiConnectionState), Ord(gKodiQueryThread.Connection.State));

            // cut of enum prefix
            gReturnValue := Copy(gReturnValue, 3, Length(gReturnValue));
          end;

        finally
          gKodiQueryThread.Connection.EndAccess();
        end;

      end else begin

        if lInfoKindParameter^.Value = 'Host' then
          // gReturnValue is already empty
        else
        if lInfoKindParameter^.Value = 'Port' then
          gReturnValue := '0'
        else
        if lInfoKindParameter^.Value = 'State' then begin
          gReturnValue := GetEnumName(TypeInfo(TKodiConnectionState), Ord(csDisconnected));

          // cut of enum prefix
          gReturnValue := Copy(gReturnValue, 3, Length(gReturnValue));
        end;

      end;

    end;
  end;

  result := PWideChar(gReturnValue);
end;

function Library_GetActivePlayer(const AParameter: PScriptFunctionImplementationParameter): PWideChar; stdcall;
begin
  gReturnValue := GetEnumName(TypeInfo(TKodiPlayerKind), Ord(pkNone));

  if Assigned(gKodiQueryThread) then
    gReturnValue := GetEnumName(TypeInfo(TKodiPlayerKind), Ord(gKodiQueryThread.ActivePlayer));

  // cut of enum prefix
  gReturnValue := Copy(gReturnValue, 3, Length(gReturnValue));

  result := PWideChar(gReturnValue);
end;

function Library_GetPlayerInfo(const AParameter: PScriptFunctionImplementationParameter): PWideChar; stdcall;
var
  lPlayerKindParameter , lInfoKindParameter: PScriptFunctionParameter;
  lPlayer: TKodiBasePlayer;

begin
  gReturnValue := '';

  if AParameter^.StructureSize >= SizeOf(TScriptFunctionImplementationParameter) then begin
    if AParameter^.ParameterCount > 1 then begin
      lPlayerKindParameter := AParameter^.ParameterList;

      lInfoKindParameter := AParameter^.ParameterList;
      Inc(lInfoKindParameter);

      if Assigned(gKodiQueryThread) then begin

        if lPlayerKindParameter^.Value = 'Audio' then
          lPlayer := gKodiQueryThread.AudioPlayer
        else
        if lPlayerKindParameter^.Value = 'Video' then
          lPlayer := gKodiQueryThread.VideoPlayer
        else
          lPlayer := gKodiQueryThread.PicturePlayer;

        lPlayer.BeginAccess();
        try

          if lInfoKindParameter^.Value = 'State' then begin
            gReturnValue := GetEnumName(TypeInfo(TKodiPlayerState), Ord(lPlayer.State));

            // cut of enum prefix
            gReturnValue := Copy(gReturnValue, 3, Length(gReturnValue));
          end else
          if lInfoKindParameter^.Value = 'CurrentItemIndex' then
            gReturnValue := IntToStr(lPlayer.CurrentItemPlaylistIndex)
          else
          if lInfoKindParameter^.Value = 'CurrentItemPosition' then
            gReturnValue := IntToStr(lPlayer.PlaybackPosition);

        finally
          lPlayer.EndAccess();
        end;

      end else begin

        if lInfoKindParameter^.Value = 'State' then begin
          gReturnValue := GetEnumName(TypeInfo(TKodiPlayerState), Ord(psUnknown));

          // cut of enum prefix
          gReturnValue := Copy(gReturnValue, 3, Length(gReturnValue));
        end else
        if lInfoKindParameter^.Value = 'CurrentItemIndex' then
          gReturnValue := '-1'
        else
        if lInfoKindParameter^.Value = 'CurrentItemPosition' then
          gReturnValue := '0';

      end;

    end;
  end;

  result := PWideChar(gReturnValue);
end;

function Library_GetPlaylistInfo(const AParameter: PScriptFunctionImplementationParameter): PWideChar; stdcall;
var
  lPlayerKindParameter: PScriptFunctionParameter;

begin
  gReturnValue := '';

  if AParameter^.StructureSize >= SizeOf(TScriptFunctionImplementationParameter) then begin
    if AParameter^.ParameterCount > 0 then begin
      lPlayerKindParameter := AParameter^.ParameterList;

      if lPlayerKindParameter^.Value = 'Audio' then
        gReturnValue := gPluginHelper.QueryPlaylist(gKodiQueryThread.AudioPlayer.Playlist, AParameter)
      else
        gReturnValue := gPluginHelper.QueryPlaylist(gKodiQueryThread.VideoPlayer.Playlist, AParameter);
    end;
  end;

  result := PWideChar(gReturnValue);
end;

function Library_GetGUIInfo(const AParameter: PScriptFunctionImplementationParameter): PWideChar; stdcall;
var
  lInfoKindParameter: PScriptFunctionParameter;

begin
  gReturnValue := '';

  if AParameter^.StructureSize >= SizeOf(TScriptFunctionImplementationParameter) then begin
    if AParameter^.ParameterCount > 0 then begin
      lInfoKindParameter := AParameter^.ParameterList;

      if lInfoKindParameter^.Value = 'Property' then begin
        if AParameter^.ParameterCount > 1 then
          gReturnValue := gPluginHelper.QueryGUIProperty(AParameter^.ParameterList[1].Value);
      end;
    end;
  end;

  result := PWideChar(gReturnValue);
end;

function Library_GetApplicationInfo(const AParameter: PScriptFunctionImplementationParameter): PWideChar; stdcall;
var
  lInfoKindParameter: PScriptFunctionParameter;

begin
  gReturnValue := '';

  if AParameter^.StructureSize >= SizeOf(TScriptFunctionImplementationParameter) then begin
    if AParameter^.ParameterCount > 0 then begin
      lInfoKindParameter := AParameter^.ParameterList;

      if lInfoKindParameter^.Value = 'Property' then begin
        if AParameter^.ParameterCount > 1 then
          gReturnValue := gPluginHelper.QueryApplicationProperty(AParameter^.ParameterList[1].Value);
      end;
    end;
  end;

  result := PWideChar(gReturnValue);
end;

function Library_GetPVRInfo(const AParameter: PScriptFunctionImplementationParameter): PWideChar; stdcall;
var
  lInfoKindParameter: PScriptFunctionParameter;

begin
  gReturnValue := '';

  if AParameter^.StructureSize >= SizeOf(TScriptFunctionImplementationParameter) then begin
    if AParameter^.ParameterCount > 0 then begin
      lInfoKindParameter := AParameter^.ParameterList;

      if lInfoKindParameter^.Value = 'Property' then begin
        if AParameter^.ParameterCount > 1 then
          gReturnValue := gPluginHelper.QueryPVRProperty(AParameter^.ParameterList[1].Value);
      end else
      if lInfoKindParameter^.Value = 'Channel' then
        gReturnValue := gPluginHelper.QueryPVRChannel(AParameter)
      else
      if lInfoKindParameter^.Value = 'CurrentProgram' then
        gReturnValue := gPluginHelper.QueryPVRCurrentProgram(AParameter);
    end;
  end;

  result := PWideChar(gReturnValue);
end;

function Library_GetTVShow(const AParameter: PScriptFunctionImplementationParameter): PWideChar; stdcall;
var
  lInfoKindParameter: PScriptFunctionParameter;

begin
  gReturnValue := '';

  if AParameter^.StructureSize >= SizeOf(TScriptFunctionImplementationParameter) then begin
    if AParameter^.ParameterCount > 0 then begin
      lInfoKindParameter := AParameter^.ParameterList;

      if lInfoKindParameter^.Value = 'TVShowDetail' then
        gReturnValue := gPluginHelper.QueryTVShowDetail(AParameter);
    end;
  end;

  result := PWideChar(gReturnValue);
end;

{$ENDREGION}

initialization
  gPluginHelper := TPluginHelper.Create();

  gKodiQueryThread := nil;

  ConnectToKodi := @Library_ConnectToKodi;
  DisconnectFromKodi := @Library_DisconnectFromKodi;
  GetConnectionInfo := @Library_GetConnectionInfo;
  GetActivePlayer := @Library_GetActivePlayer;
  GetPlayerInfo := @Library_GetPlayerInfo;
  GetPlaylistInfo := @Library_GetPlaylistInfo;
  GetGUIInfo := @Library_GetGUIInfo;
  GetPVRInfo := @Library_GetPVRInfo;
  GetTVShowInfo := @Library_GetTVShow;
  GetApplicationInfo := @Library_GetApplicationInfo;

finalization
  gPluginHelper.FreeKodiQueryThread();

  gPluginHelper.Free;

end.


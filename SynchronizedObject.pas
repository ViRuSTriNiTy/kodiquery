//
// Synchronizedo object classes
//
// $Id$
//

unit SynchronizedObject;

interface

uses
  Classes, // for TStrings
  SyncObjs, // for TCriticalSection
  TypInfo, // for RTTI
  JSON; // for TJSON classes

type
  TKodiSynchronizedQueryResult = class(TObject)
    strict private
      fSynchronizationMutex: TCriticalSection;

    public
      constructor Create(); virtual;
      destructor Destroy(); override;

      // the folling methods should be called before and after acessing a
      // property of derived classes
      procedure BeginAccess(); inline;
      procedure EndAccess(); inline;
  end;

  {$M+}
  TKodiPropertyQueryResult = class(TKodiSynchronizedQueryResult)
    protected
      function IsUnicodeStringProperty(const APropertyType: TTypeKind): Boolean;

    public
      function GetProperty(const AName: UnicodeString): UnicodeString;
  end;
  {$M-}

  TKodiGetPropertiesResult = class(TKodiPropertyQueryResult)
    public
      procedure AfterConstruction(); override;
      procedure Reset();
      function IsReset(): Boolean;
      procedure SetProperty(const AMember: TJSONMember); virtual;

      class procedure GetSupportedFields(const AList: TStrings); virtual; abstract;
  end;

implementation

uses
  SysUtils; // for IntToStr()

{$REGION ' TKodiSynchronizedQueryResult '}

constructor TKodiSynchronizedQueryResult.Create();
begin
  inherited;
end;

destructor TKodiSynchronizedQueryResult.Destroy();
begin
  if Assigned(fSynchronizationMutex) then
    fSynchronizationMutex.Free;

  inherited;
end;

procedure TKodiSynchronizedQueryResult.BeginAccess();
begin
  if not Assigned(fSynchronizationMutex) then
    fSynchronizationMutex := SyncObjs.TCriticalSection.Create();

  fSynchronizationMutex.Acquire();
end;

procedure TKodiSynchronizedQueryResult.EndAccess();
begin
  if Assigned(fSynchronizationMutex) then
    fSynchronizationMutex.Release();
end;

{$ENDREGION}

{$REGION ' TKodiPropertyQueryResult '}

function TKodiPropertyQueryResult.IsUnicodeStringProperty(const APropertyType: TTypeKind): Boolean;
begin
  result := APropertyType = tkUString;
end;

function TKodiPropertyQueryResult.GetProperty(const AName: UnicodeString): UnicodeString;
var
  lPropInfo: PPropInfo;

begin
  lPropInfo := GetPropInfo(Self.ClassInfo, AName, tkProperties);
  if Assigned(lPropInfo) and Assigned(lPropInfo^.GetProc) then begin

    if IsUnicodeStringProperty(lPropInfo^.PropType^.Kind) then
      result := GetUnicodeStrProp(Self, lPropInfo)
    else begin

      case lPropInfo^.PropType^.Kind of
        tkInteger:
          result := IntToStr(GetOrdProp(Self, lPropInfo));
      end;

    end;

  end else
    result := '';
end;

{$ENDREGION}

{$REGION ' TKodiGetPropertiesResult '}

procedure TKodiGetPropertiesResult.AfterConstruction();
begin
  inherited;

  Reset();
end;

procedure TKodiGetPropertiesResult.Reset();
var
  lTypeData: PTypeData;
  lPropList: PPropList;
  lPropInfo: PPropInfo;
  lPropCount, c: Integer;

begin
  lTypeData := GetTypeData(Self.ClassInfo);
  if Assigned(lTypeData) then begin

    lPropCount := lTypeData^.PropCount;
    if lPropCount > 0 then begin

      GetMem(lPropList, lPropCount * SizeOf(lPropList));
      try
        c := GetPropList(Self.ClassInfo, tkProperties, lPropList, false);

        while c > 0 do begin
          lPropInfo := lPropList^[c - 1];

          if IsUnicodeStringProperty(lPropInfo^.PropType^.Kind) then
            SetUnicodeStrProp(Self, lPropInfo, '')
          else begin

            case lPropInfo^.PropType^.Kind of
              tkInteger, tkInt64, tkEnumeration, tkQWord:
                SetOrdProp(Self, lPropInfo, lPropInfo^.Default);
            end;
          end;

          c := c - 1;
        end;

      finally
        FreeMem(lPropList, lPropCount * SizeOf(lPropList));
      end;

    end;
  end;
end;

function TKodiGetPropertiesResult.IsReset(): Boolean;
var
  lTypeData: PTypeData;
  lPropList: PPropList;
  lPropInfo: PPropInfo;
  lPropCount, c: Integer;

begin
  result := true;

  lTypeData := GetTypeData(Self.ClassInfo);
  if Assigned(lTypeData) then begin

    lPropCount := lTypeData^.PropCount;
    if lPropCount > 0 then begin

      GetMem(lPropList, lPropCount * SizeOf(lPropList));
      try
        c := GetPropList(Self.ClassInfo, tkProperties, lPropList, false);

        while c > 0 do begin
          lPropInfo := lPropList^[c - 1];

          if IsUnicodeStringProperty(lPropInfo^.PropType^.Kind) then
            result := result and (GetUnicodeStrProp(Self, lPropInfo) = '')
          else begin

            case lPropInfo^.PropType^.Kind of
              tkInteger:
                result := result and (GetOrdProp(Self, lPropInfo) = lPropInfo^.Default);
            end;

          end;

          c := c - 1;
        end;

      finally
        FreeMem(lPropList, lPropCount * SizeOf(lPropList));
      end;

    end;
  end;
end;

procedure TKodiGetPropertiesResult.SetProperty(const AMember: TJSONMember);
var
  lPropInfo: PPropInfo;
  lPropertyName: String;
  lPropertyValueStr: UnicodeString;
  lPropertyValueInt: Int64;

begin
  lPropertyName := AMember.Name;

  lPropInfo := GetPropInfo(Self.ClassInfo, lPropertyName, tkProperties);
  if Assigned(lPropInfo) and Assigned(lPropInfo^.SetProc) then begin

    if IsUnicodeStringProperty(lPropInfo^.PropType^.Kind) then begin
      lPropertyValueStr := AMember.ToSimpleValue();

      SetUnicodeStrProp(Self, lPropInfo, lPropertyValueStr);
    end else begin

      case lPropInfo^.PropType^.Kind of
        tkInteger:
          begin
            if AMember is TJSONFloatMember then
              lPropertyValueInt := TJSONFloatMember(AMember).ToInt64()
            else
              lPropertyValueInt := StrToInt64Def(AMember.ToSimpleValue(), 0);

            SetOrdProp(Self, lPropInfo, lPropertyValueInt);
          end;

        tkBool:
          begin
            if AMember is TJSONBooleanMember then
              lPropertyValueInt := Ord(TJSONBooleanMember(AMember).Value)
            else
              lPropertyValueInt := StrToIntDef(AMember.ToSimpleValue(), 0);

            SetOrdProp(Self, lPropInfo, lPropertyValueInt);
          end;
      end;

    end;

  end;
end;

{$ENDREGION}

end.


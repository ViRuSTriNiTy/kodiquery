# Kodi Query Plugin for LCDHype #
Copyright (C) 2016 ViRuSTriNiTy <cradle-of-mail at gmx.de>

## Preamble ##
This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Dependencies ##

- Jzn, <https://bitbucket.org/ViRuSTriNiTy/jzn>
- Oppu, <https://bitbucket.org/ViRuSTriNiTy/oppu>

## Introduction ##
This plugin queries data from Kodi via the JSON-RPC interface for further processing in LCDHype.

## Changelog ##

### Version 0.3.2.12 ###
- update to support Kodi Jarvis

### Version 0.3.2.3 ###
- added PVR status and channel list command
- added GUI commands
  - %Plugin.Kodi.GUI.CurrentWindowID()
  - %Plugin.Kodi.GUI.CurrentWindowLabel()
- added support for Filename parameter in %Plugin.Kodi.VideoPlayer.Playlist.Item()
- updated queries for using plugin with Kodi Isengard
  
### Version 0.2 ###
- update for compatibility with Kodi Eden

### Version 0.1 ###
- first release of Kodi Query plugin for Kodi Dharma
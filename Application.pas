unit Application;

interface

uses
  Classes, // for TStrings
  SynchronizedObject;

type
  TKodiApplicationProperties = class(TKodiGetPropertiesResult)
    strict private
      fMuted: Boolean;
      fVolume: Integer;

    public
      class procedure GetSupportedFields(const AList: TStrings); override;

    published
      property Muted: Boolean read fMuted write fMuted default false;
      property Volume: Integer read fVolume write fVolume default 0;
  end;

  TKodiApplication = class(TObject)
    private
      fProperties: TKodiApplicationProperties;

    public
      constructor Create();
      destructor Destroy(); override;

      property Properties: TKodiApplicationProperties read fProperties;
  end;

implementation

{$REGION ' TKodiApplicationProperties '}

class procedure TKodiApplicationProperties.GetSupportedFields(const AList: TStrings);
begin
  AList.Add('muted');
  AList.Add('volume');
end;

{$ENDREGION}

{$REGION ' TKodiApplication '}

constructor TKodiApplication.Create();
begin
  inherited;

  fProperties := TKodiApplicationProperties.Create();
end;

destructor TKodiApplication.Destroy();
begin
  fProperties.Free;

  inherited;
end;

{$ENDREGION}

end.

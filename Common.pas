//
// Common stuff
//
// $Id$
//

unit Common;

interface

uses
  {$IFDEF FPC}
    gutil, // for TLess<>
    FGL // for TFPGMap
  {$ELSE}
    Generics.Collections
  {$ENDIF};

type
  {$IFDEF FPC}
    TIntCompare = specialize TLess<Integer>;

    TIDMap = specialize TFPGMap<Integer, Boolean>;
  {$ELSE}
    TIDMap = TDictionary<Integer, Boolean>;
  {$ENDIF}

implementation

end.


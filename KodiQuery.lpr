library KodiQuery;

{$I ScriptEngineCompilers.inc}

uses
  Main, KodiHelpers, Channel, TVShow, Common, SynchronizedObject,
  ScriptEngineInterface, Application, lnetbase;

{$R *.res}

exports GetID;
exports Init;
exports CleanUp;

exports ConnectToKodi;
exports DisconnectFromKodi;
exports GetConnectionInfo;
exports GetActivePlayer;
exports GetPlayerInfo;
exports GetPlaylistInfo;
exports GetGUIInfo;
exports GetPVRInfo;
exports GetTVShowInfo;
exports GetApplicationInfo;

end.


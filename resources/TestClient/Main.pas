unit Main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls, KodiHelpers;

type

  { TMainForm }

  TMainForm = class(TForm)
    Memo1: TMemo;
    Timer1: TTimer;
    Button1: TButton;
    Button2: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
      qt: TKodiQueryThread;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

uses
  Channel,
  TVShow;

{$R *.lfm}

procedure TMainForm.Button1Click(Sender: TObject);
begin
  qt.Connect('127.0.0.1', 9090);
end;

procedure TMainForm.Button2Click(Sender: TObject);
begin
  qt.Disconnect();
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  qt := TKodiQueryThread.Create();
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  FreeAndNil(qt);
end;

procedure TMainForm.Timer1Timer(Sender: TObject);
var
  i: Integer;
  lPlaylistItem: TKodiAudioPlayerPlayableItem;
  lPlaylistItemV: TKodiVideoPlayerPlayableItem;
  lDateTime: TDateTime;
  lChannel: TKodiPVRChannel;
  lTVShow: TKodiTVShow;
  s: AnsiString;

begin
  if Assigned(qt) then begin
    Memo1.Lines.BeginUpdate();
    Memo1.Lines.Clear();

    qt.Connection.BeginAccess();
    try
      Memo1.Lines.Add('Connection to Kodi');
      Memo1.Lines.Add(qt.Connection.ToString());
      Memo1.Lines.Add('BufferedResponseObjects=' + IntToStr(qt.JSONClient.ResponseObjectBufferSize));
      Memo1.Lines.Add('');
    finally
      qt.Connection.EndAccess();
    end;

    qt.Application.Properties.BeginAccess();
    try
      Memo1.Lines.Add('Application.Muted=' + IntToStr(Ord(qt.Application.Properties.Muted)));
      Memo1.Lines.Add('Application.Volume=' + IntToStr(qt.Application.Properties.Volume) + '/100');
      Memo1.Lines.Add('');
    finally
      qt.Application.Properties.EndAccess();
    end;

    if qt.AudioPlayer.State in [psPlaying, psPaused] then begin
      Memo1.Lines.Add('Audio active');

      qt.AudioPlayer.Playlist.BeginAccess();
      try
        Memo1.Lines.Add('Current item');
        Memo1.Lines.Add('Index=' + IntTostr(qt.AudioPlayer.CurrentItemPlaylistIndex));
        Memo1.Lines.Add('Position=' + IntToStr(qt.AudioPlayer.PlaybackPosition));
        Memo1.Lines.Add('');

        for i := 0 to qt.AudioPlayer.Playlist.Count - 1 do begin
          lPlaylistItem := qt.AudioPlayer.Playlist[i];

          Memo1.Lines.Add('Playlist item ' + IntToStr(i));
          Memo1.Lines.Add('Index=' + IntToStr(lPlaylistItem.Index));
          Memo1.Lines.Add('Length=' + IntToStr(lPlaylistItem.Length));
          Memo1.Lines.Add('Title=' + UTF8Encode(lPlaylistItem.Title));
          Memo1.Lines.Add('Album=' + UTF8Encode(lPlaylistItem.Album));
          Memo1.Lines.Add('Artist=' + UTF8Encode(lPlaylistItem.Artist));
          Memo1.Lines.Add('Comment=' + UTF8Encode(lPlaylistItem.Comment));
          Memo1.Lines.Add('Genre=' + UTF8Encode(lPlaylistItem.Genre));
          Memo1.Lines.Add('Filename=' + UTF8Encode(lPlaylistItem.Filename));
          Memo1.Lines.Add('');
        end;
      finally
        qt.AudioPlayer.Playlist.EndAccess();
      end;

    end else
      Memo1.Lines.Add('Audio inactive');

    if qt.VideoPlayer.State in [psPlaying, psPaused] then begin
      Memo1.Lines.Add('Video active');

      qt.VideoPlayer.Playlist.BeginAccess();
      try
        Memo1.Lines.Add('Current item');
        Memo1.Lines.Add('Index=' + IntTostr(qt.VideoPlayer.CurrentItemPlaylistIndex));
        Memo1.Lines.Add('Position=' + IntToStr(qt.VideoPlayer.PlaybackPosition));
        Memo1.Lines.Add('PlaylistID=' + IntToStr(qt.VideoPlayer.PlaylistID));

        for i := 0 to qt.VideoPlayer.Playlist.Count - 1 do begin
          lPlaylistItemV := qt.VideoPlayer.Playlist[i];

          Memo1.Lines.Add('Playlist item ' + IntToStr(i));
          Memo1.Lines.Add('Index=' + IntToStr(lPlaylistItemV.Index));
          Memo1.Lines.Add('Length=' + IntToStr(lPlaylistItemV.Length));

          if lPlaylistItemV.TVShowID <> -1 then begin
            qt.TVShows.BeginAccess();
            try
              if qt.TVShows.GetTVShowByID(lPlaylistItemV.TVShowID, lTVShow) then begin
                Memo1.Lines.Add('TVShowTitle=' + UTF8Encode(lTVShow.Title));
                Memo1.Lines.Add('TVShowSeason=' + IntToStr(lPlaylistItemV.Season));
                Memo1.Lines.Add('TVShowEpisodeNumber=' + IntToStr(lPlaylistItemV.EpisodeNumber));
                Memo1.Lines.Add('TVShowEpisodeTitle=' + UTF8Encode(lPlaylistItemV.Title));
              end;

            finally
              qt.TVShows.EndAccess();
            end;

          end else
            Memo1.Lines.Add('Title=' + UTF8Encode(lPlaylistItemV.Title));

          Memo1.Lines.Add('Genre=' + UTF8Encode(lPlaylistItemV.Genre));
          Memo1.Lines.Add('Director=' + UTF8Encode(lPlaylistItemV.Director));
          Memo1.Lines.Add('Tagline=' + UTF8Encode(lPlaylistItemV.Tagline));
          Memo1.Lines.Add('Plot=' + UTF8Encode(lPlaylistItemV.Plot));
          Memo1.Lines.Add('Year=' + lPlaylistItemV.Year);
          Memo1.Lines.Add('Writer=' + UTF8Encode(lPlaylistItemV.Writer));
          Memo1.Lines.Add('MPAA=' + UTF8Encode(lPlaylistItemV.MPAA));
          Memo1.Lines.Add('Filename=' + UTF8Encode(lPlaylistItemV.Filename));
          Memo1.Lines.Add('');
        end;
      finally
        qt.VideoPlayer.Playlist.EndAccess();
      end;

    end else
      Memo1.Lines.Add('Video inactive');

    if qt.PVR.Properties.Available then begin
      Memo1.Lines.Add('PVR available');

      qt.PVR.Properties.BeginAccess();
      qt.PVR.CurrentProgram.BeginAccess();
      try
        Memo1.Lines.Add('Scanning=' + IntTostr(Ord(qt.PVR.Properties.Scanning)));
        Memo1.Lines.Add('Recording=' + IntTostr(Ord(qt.PVR.Properties.Recording)));

        Memo1.Lines.Add('Current program');
        Memo1.Lines.Add('State=' + coKodiPlayerStateStr[qt.PVR.CurrentProgram.State]);

        Memo1.Lines.Add('ChannelID=' + IntTostr(qt.PVR.CurrentProgram.ChannelID));
        if qt.PVR.CurrentProgram.ChannelID > 0 then begin
          qt.PVR.ChannelList.BeginAccess();
          try
            if qt.PVR.ChannelList.GetChannelByID(qt.PVR.CurrentProgram.ChannelID, lChannel) then
              Memo1.Lines.Add('ChannelName=' + UTF8Encode(lChannel.Name));

          finally
            qt.PVR.ChannelList.EndAccess();
          end;
        end;

        Memo1.Lines.Add('Name=' + UTF8Encode(qt.PVR.CurrentProgram.Name));

        lDateTime := qt.PVR.CurrentProgram.MsToDateTime(qt.PVR.CurrentProgram.StartTime);
        DateTimeToString(s, 'dd.mm.yyyy, hh:mm:ss', lDateTime);

        Memo1.Lines.Add('StartTime=' + s);

        lDateTime := qt.PVR.CurrentProgram.MsToDateTime(qt.PVR.CurrentProgram.EndTime);
        DateTimeToString(s, 'dd.mm.yyyy, hh:mm:ss', lDateTime);

        Memo1.Lines.Add('EndTime=' + s);
        Memo1.Lines.Add('Length=' + IntToStr(qt.PVR.CurrentProgram.Length));
        Memo1.Lines.Add('Position=' + IntToStr(qt.PVR.CurrentProgram.Position));
        Memo1.Lines.Add('Genre=' + UTF8Encode(qt.PVR.CurrentProgram.Genre));

      finally
        qt.PVR.CurrentProgram.EndAccess();
        qt.PVR.Properties.EndAccess();
      end;

    end else
      Memo1.Lines.Add('PVR not available');

    if qt.PicturePlayer.State in [psPlaying, psPaused] then begin
      Memo1.Lines.Add('Picture active');

    end else
      Memo1.Lines.Add('Picture inactive');

    Memo1.Lines.EndUpdate();
  end;
end;

end.


program KodiTestClient;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, lnetbase, Main, KodiHelpers, TVShow, Channel, SynchronizedObject,
  Common, Application;

{$R *.res}

begin
  RequireDerivedFormResource := True;
  Forms.Application.Initialize;
  Forms.Application.CreateForm(TMainForm, MainForm);
  Forms.Application.Run;
end.


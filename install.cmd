@echo off
SETLOCAL
pushd
cd build

set TARGET_DIR=%LCDHYPE_INSTALLATION%\plugins\KodiQuery

echo Copying plugin files from %CD% to %TARGET_DIR%... > ..\install.log
copy *.* "%TARGET_DIR%" >> ..\install.log

popd
ENDLOCAL

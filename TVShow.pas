//
// TV show classes
//
// $Id$
//

unit TVShow;

interface

uses
  Classes,
  Contnrs,              // for TObjectList
  {$IFDEF FPC}
    gmap,
  {$ELSE}
    Generics.Collections,
  {$ENDIF}
  Common, // for TIntCompare
  SynchronizedObject;

type
  TKodiTVShow = class;

  {$IFDEF FPC}
    TKodiTVShowIDTVShowListItemMap = specialize TMap<Integer, TKodiTVShow, TIntCompare>;
  {$ELSE}
    TKodiTVShowIDTVShowListItemMap = TDictionary<Integer, TKodiTVShow>;
  {$ENDIF}

  TKodiTVShow = class(TKodiGetPropertiesResult)
    strict private
      fID: Integer;
      fTitle: UnicodeString;
      fTVShowIDTVShowListItemMap: TKodiTVShowIDTVShowListItemMap;

      procedure SetID(const AValue: Integer);

      procedure RegisterToTVShowIDTVShowListItemMap();
      procedure UnregisterFromTVShowIDTVShowListItemMap();

    private
      procedure SetTVShowIDTVShowListItemMap(const AMap: TKodiTVShowIDTVShowListItemMap);

    public
      destructor Destroy(); override;

      class procedure GetSupportedFields(const AList: TStrings); override;

      property ID: Integer read fID write SetID;

    published
      property Title: UnicodeString read fTitle write fTitle;
  end;

  TKodiTVShowListItemList = class(TObjectList)
    strict private
      fTVShowIDTVShowListItemMap: TKodiTVShowIDTVShowListItemMap;

    protected
      procedure Notify(APtr: Pointer; AAction: TListNotification); override;

    public
      constructor Create();
      destructor Destroy(); override;

      function GetTVShowByID(const ATVShowID: Integer; out ATVShow: TKodiTVShow): Boolean;
  end;

  TKodiTVShowList = class(TKodiSynchronizedQueryResult)
    strict private
      function GetCount(): Integer;
      procedure SetCount(const AValue: Integer);

    private
      fItemList: TKodiTVShowListItemList;

      function GetItem(const AIndex: Integer): TKodiTVShow;
      procedure SetItem(const AIndex: Integer; const AItem: TKodiTVShow);

    public
      constructor Create(); override;
      destructor Destroy(); override;

      procedure Reset();
      function IsReset(): Boolean;

      procedure Add(const AItem: TKodiTVShow);
      procedure Remove(const AItems: TObjectList);
      procedure CopyTo(const AObjectList: TObjectList);
      function GetTVShowByID(const ATVShowID: Integer; out ATVShow: TKodiTVShow): Boolean;

      property Count: Integer read GetCount write SetCount;
      property Items[const AIndex: Integer]: TKodiTVShow read GetItem write SetItem; default;
  end;

implementation

uses
  SysUtils; // for FreeAndNil()

{$REGION ' TKodiTVShow '}

destructor TKodiTVShow.Destroy();
begin
  UnregisterFromTVShowIDTVShowListItemMap();

  inherited;
end;

class procedure TKodiTVShow.GetSupportedFields(const AList: TStrings);
begin
  AList.Add('title');
end;

procedure TKodiTVShow.RegisterToTVShowIDTVShowListItemMap();
{$IFDEF FPC}
var
  i: TKodiTVShowIDTVShowListItemMap.TIterator;
{$ENDIF}

begin
  if fTVShowIDTVShowListItemMap <> nil then begin
    i := fTVShowIDTVShowListItemMap.Find(fID);
    if i <> nil then begin
      i.Value := self;

      i.Free;
    end else
      fTVShowIDTVShowListItemMap.Insert(fID, self);
  end;
end;

procedure TKodiTVShow.UnregisterFromTVShowIDTVShowListItemMap();
{$IFDEF FPC}
var
  i: TKodiTVShowIDTVShowListItemMap.TIterator;
{$ENDIF}

begin
  if fTVShowIDTVShowListItemMap <> nil then begin
    i := fTVShowIDTVShowListItemMap.Find(fID);
    if i <> nil then begin
      fTVShowIDTVShowListItemMap.Delete(i.Key);

      i.Free;
    end;
  end;
end;

procedure TKodiTVShow.SetID(const AValue: Integer);
begin
  if AValue <> fID then begin
    UnregisterFromTVShowIDTVShowListItemMap();

    fID := AValue;

    RegisterToTVShowIDTVShowListItemMap();
  end;
end;

procedure TKodiTVShow.SetTVShowIDTVShowListItemMap(const AMap: TKodiTVShowIDTVShowListItemMap);
begin
  UnregisterFromTVShowIDTVShowListItemMap();

  fTVShowIDTVShowListItemMap := AMap;

  RegisterToTVShowIDTVShowListItemMap();
end;

{$ENDREGION}

{$REGION ' TKodiTVShowListItemList '}

constructor TKodiTVShowListItemList.Create();
begin
  inherited Create(true);

  fTVShowIDTVShowListItemMap := TKodiTVShowIDTVShowListItemMap.Create();
end;

destructor TKodiTVShowListItemList.Destroy();
var
  i, l: Integer;

begin
  l := Count - 1;
  for i := 0 to l do
    TKodiTVShow(GetItem(i)).SetTVShowIDTVShowListItemMap(nil);

  FreeAndNil(fTVShowIDTVShowListItemMap);

  inherited;
end;

procedure TKodiTVShowListItemList.Notify(APtr: Pointer; AAction: TListNotification);
begin
  if AAction in [lnDeleted, lnExtracted] then begin
    if TObject(APtr) is TKodiTVShow then
      TKodiTVShow(APtr).SetTVShowIDTVShowListItemMap(nil);
  end;

  inherited Notify(APtr, AAction);

  if AAction = lnAdded then begin
    if TObject(APtr) is TKodiTVShow then
      TKodiTVShow(APtr).SetTVShowIDTVShowListItemMap(fTVShowIDTVShowListItemMap);
  end;
end;

function TKodiTVShowListItemList.GetTVShowByID(const ATVShowID: Integer;
  out ATVShow: TKodiTVShow): Boolean;
{$IFDEF FPC}
var
  i: TKodiTVShowIDTVShowListItemMap.TIterator;
{$ENDIF}

begin
  result := false;

  if fTVShowIDTVShowListItemMap <> nil then begin
    i := fTVShowIDTVShowListItemMap.Find(ATVShowID);
    if i <> nil then begin
      ATVShow := i.Value;

      i.Free;

      result := ATVShow <> nil;
    end;
  end;
end;

{$ENDREGION}

{$REGION ' TKodiTVShowList '}

constructor TKodiTVShowList.Create();
begin
  inherited;

  fItemList := TKodiTVShowListItemList.Create();
end;

destructor TKodiTVShowList.Destroy();
begin
  Self.BeginAccess();
  try
    fItemList.Free;
  finally
    Self.EndAccess();
  end;

  inherited;
end;

procedure TKodiTVShowList.Add(const AItem: TKodiTVShow);
begin
  fItemList.Add(AItem);
end;

procedure TKodiTVShowList.Remove(const AItems: TObjectList);
var
  i, l: Integer;

begin
  l := AItems.Count - 1;
  for i := 0 to l do
    fItemList.Remove(AItems[i]);
end;

procedure TKodiTVShowList.CopyTo(const AObjectList: TObjectList);
begin
  AObjectList.Assign(fItemList, laCopy);
end;

function TKodiTVShowList.GetTVShowByID(const ATVShowID: Integer; out ATVShow: TKodiTVShow): Boolean;
begin
  if Assigned(fItemList) then
    result := fItemList.GetTVShowByID(ATVShowID, ATVShow)
  else
    result := false;
end;

function TKodiTVShowList.GetCount(): Integer;
begin
  if Assigned(fItemList) then
    result := fItemList.Count
  else
    result := 0;
end;

procedure TKodiTVShowList.SetCount(const AValue: Integer);
begin
  if Assigned(fItemList) then
    fItemList.Count := AValue;
end;

function TKodiTVShowList.GetItem(const AIndex: Integer): TKodiTVShow;
begin
  if (AIndex >= 0) and (AIndex < fItemList.Count) and Assigned(fItemList) then
    result := TKodiTVShow(fItemList[AIndex])
  else
    result := nil;
end;

procedure TKodiTVShowList.SetItem(const AIndex: Integer; const AItem: TKodiTVShow);
begin
  if (AIndex >= 0) and (AIndex < fItemList.Count) and Assigned(fItemList) then
    fItemList[AIndex] := AItem;
end;

procedure TKodiTVShowList.Reset();
begin
  if Assigned(fItemList) then
    fItemList.Clear();
end;

function TKodiTVShowList.IsReset(): Boolean;
begin
  if Assigned(fItemList) then
    result := fItemList.Count = 0
  else
    result := true;
end;

{$ENDREGION}

end.


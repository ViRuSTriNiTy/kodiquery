//
// Kodi helper unit
//
// $Id$
//

unit KodiHelpers;

interface

uses
  JSON,                 // for TJSON classes
  JSONRPC,              // for TJSONRPC classes
  SyncObjs,             // for TEvent
  Classes,              // for TThread
  Contnrs,              // for TObjectList
  SysUtils,             // for TFormatSettings
  SynchronizedObject,
  Channel,
  TVShow,
  Application;

const
  coKodiResponseTimeout = 250; // ms, the lower the value the more often Kodi cannot respond in time, 250 seems to be ok

type
  TKodiPlayerKind = (
    pkNone,
    pkAudio,
    pkVideo,
    pkPicture
  );

  TKodiPlayerState = (
    psUnknown,   // state that an error is occured while determining state
    psInactive,
    psPaused,
    psPlaying
  );

const
  coKodiPlayerStateStr: array[TKodiPlayerState] of UnicodeString =
    ('Unknown', 'Inactive', 'Paused', 'Playing');

type
  TKodiConnectionState = (
    csDisconnected,
    csConnecting,
    csConnected,
    csDisconnecting,
    csErrorOccured
  );

  TKodiPlayableItem = class(TKodiGetPropertiesResult)
    strict private
      fIndex: Integer;

    published
      property Index: Integer read fIndex write fIndex default -1;
  end;

  TKodiPlayableItemClass = class of TKodiPlayableItem;

  TKodiPlaylist = class(TKodiSynchronizedQueryResult)
    strict private
      fItemList: TObjectList;

      function GetCount(): Integer;
      procedure SetCount(const AValue: Integer);

    private
      function GetItem(const AIndex: Integer): TKodiPlayableItem;
      procedure SetItem(const AIndex: Integer; const AItem: TKodiPlayableItem);

      procedure Reset();
      function IsReset(): Boolean;

    public
      constructor Create(); override;
      destructor Destroy(); override;

      function GetItemClass(): TKodiPlayableItemClass; virtual; abstract;

      property Count: Integer read GetCount write SetCount;
      property Items[const AIndex: Integer]: TKodiPlayableItem read GetItem write SetItem; default;
  end;

  TKodiPlaylistClass = class of TKodiPlaylist;

  TKodiBasePlayer = class(TKodiSynchronizedQueryResult)
    public
      State: TKodiPlayerState;
      ID: Integer;
      PlaylistID: Integer;
      CurrentItemPlaylistIndex: Integer;
      PlaybackPosition: LongWord; // ms

      function GetKind(): TKodiPlayerKind; virtual; abstract;
      function GetKodiPlayerType(): UnicodeString;
  end;

  TKodiPlayerWithPlaylist = class(TKodiBasePlayer)
    strict private
      fPlaylist: TKodiPlaylist;

    protected
      function GetPlaylistClass(): TKodiPlaylistClass; virtual; abstract;

    public
      constructor Create(); override;
      destructor Destroy(); override;

      property Playlist: TKodiPlaylist read fPlaylist;
  end;

  TKodiPlayableFileItem = class(TKodiPlayableItem)
    strict private
      fTitle: UnicodeString;
      fFilename: UnicodeString;

    public
      procedure SetProperty(const AMember: TJSONMember); override;
      class procedure GetSupportedFields(const AList: TStrings); override;

    published
      property Title: UnicodeString read fTitle write fTitle;
      property Filename: UnicodeString read fFilename write fFilename;
  end;

  TKodiAudioPlayerPlayableItem = class(TKodiPlayableFileItem)
    strict private
      fLength: LongWord;
      fAlbum: UnicodeString;
      fArtist: UnicodeString;
      fComment: UnicodeString;
      fGenre: UnicodeString;

    public
      procedure SetProperty(const AMember: TJSONMember); override;
      class procedure GetSupportedFields(const AList: TStrings); override;

    published
      property Length: LongWord read fLength write fLength default 0;
      property Album: UnicodeString read fAlbum write fAlbum;
      property Artist: UnicodeString read fArtist write fArtist;
      property Comment: UnicodeString read fComment write fComment;
      property Genre: UnicodeString read fGenre write fGenre;
  end;

  TKodiAudioPlayerPlaylist = class(TKodiPlaylist)
    strict private
      function GetItem(const AIndex: Integer): TKodiAudioPlayerPlayableItem;
      procedure SetItem(const AIndex: Integer; const AItem: TKodiAudioPlayerPlayableItem);

    public
      function GetItemClass(): TKodiPlayableItemClass; override;

      property Items[const AIndex: Integer]: TKodiAudioPlayerPlayableItem read GetItem write SetItem; default;
  end;

  TKodiAudioPlayer = class(TKodiPlayerWithPlaylist)
    strict private
      function GetPlaylist(): TKodiAudioPlayerPlaylist;

    protected
      function GetPlaylistClass(): TKodiPlaylistClass; override;

    public
      function GetKind(): TKodiPlayerKind; override;

      property Playlist: TKodiAudioPlayerPlaylist read GetPlaylist;
  end;

  TKodiVideoPlayerPlayableItem = class(TKodiPlayableFileItem)
    strict private
      fLength: LongWord;
      fGenre: UnicodeString;
      fDirector: UnicodeString;
      fTagline: UnicodeString;
      fPlot: UnicodeString;
      fYear: UnicodeString;
      fWriter: UnicodeString;
      fMPAA: UnicodeString;
      fTVShowID: Integer;
      fSeason: Integer;
      fEpisodeNumber: Integer;

    public
      procedure SetProperty(const AMember: TJSONMember); override;
      class procedure GetSupportedFields(const AList: TStrings); override;

    published
      property Length: LongWord read fLength write fLength default 0;
      property Genre: UnicodeString read fGenre write fGenre;
      property Director: UnicodeString read fDirector write fDirector;
      property Tagline: UnicodeString read fTagline write fTagline;
      property Plot: UnicodeString read fPlot write fPlot;
      property Year: UnicodeString read fYear write fYear;
      property Writer: UnicodeString read fWriter write fWriter;
      property MPAA: UnicodeString read fMPAA write fMPAA;
      property TVShowID: Integer read fTVShowID write fTVShowID;
      property Season: Integer read fSeason write fSeason;
      property EpisodeNumber: Integer read fEpisodeNumber write fEpisodeNumber;
  end;

  TKodiVideoPlayerPlaylist = class(TKodiPlaylist)
    strict private
      function GetItem(const AIndex: Integer): TKodiVideoPlayerPlayableItem;
      procedure SetItem(const AIndex: Integer; const AItem: TKodiVideoPlayerPlayableItem);

    public
      function GetItemClass(): TKodiPlayableItemClass; override;

      property Items[const AIndex: Integer]: TKodiVideoPlayerPlayableItem read GetItem write SetItem; default;
  end;

  TKodiVideoPlayer = class(TKodiPlayerWithPlaylist)
    strict private
      function GetPlaylist(): TKodiVideoPlayerPlaylist;

    protected
      function GetPlaylistClass(): TKodiPlaylistClass; override;

    public
      function GetKind(): TKodiPlayerKind; override;

      property Playlist: TKodiVideoPlayerPlaylist read GetPlaylist;
  end;


  TKodiPicturePlayerPlayableItem = class(TKodiPlayableItem)
  end;

  TKodiPicturePlayer = class(TKodiBasePlayer) // TKodiBasePlayer as picture player has no playlist
    public
      function GetKind(): TKodiPlayerKind; override;
  end;

  TKodiConnectionCommand = (
    ccNone,
    ccConnect,
    ccDisconnect,
    ccReconnect
  );

  TKodiConnection = class(TKodiSynchronizedQueryResult)
    public
      // updated on connect / disconnect request
      Command: TKodiConnectionCommand;
      Host: UnicodeString;
      Port: LongWord;

      // updated by Execute()
      State: TKodiConnectionState;

      function ToString(): String; override;
  end;

  TKodiGUIProperties = class(TKodiGetPropertiesResult)
    strict private
      fCurrentWindowLabel: UnicodeString;
      fCurrentWindowID: Integer; // http://kodi.wiki/view/Window_IDs
      fCurrentControl: UnicodeString;

    public
      procedure SetProperty(const AMember: TJSONMember); override;

      class procedure GetSupportedFields(const AList: TStrings); override;

    published
      property CurrentWindowLabel: UnicodeString read fCurrentWindowLabel write fCurrentWindowLabel;
      property CurrentWindowID: Integer read fCurrentWindowID write fCurrentWindowID default 0;
      property CurrentControl: UnicodeString read fCurrentControl write fCurrentControl;
  end;

  TKodiGUI = class(TObject)
    private
      fProperties: TKodiGUIProperties;

    public
      constructor Create();
      destructor Destroy(); override;

      property Properties: TKodiGUIProperties read fProperties;
  end;

  TKodiPVRProperties = class(TKodiGetPropertiesResult)
    strict private
      fAvailable: Boolean;
      fRecording: Boolean;
      fScanning: Boolean;

    public
      class procedure GetSupportedFields(const AList: TStrings); override;

    published
      property Available: Boolean read fAvailable write fAvailable default false;
      property Recording: Boolean read fRecording write fRecording default false;
      property Scanning: Boolean read fScanning write fScanning default false;
  end;

  TKodiPVRCurrentProgram = class(TKodiGetPropertiesResult)
    strict private
      fTimeStrFormatSettings: TFormatSettings;

      fState: TKodiPlayerState;
      fChannelID: Int64;
      fName: UnicodeString;
      fStartTime: UInt64;
      fEndTime: UInt64;
      fPosition: LongWord;
      fLength: LongWord;
      fGenre: UnicodeString;

      function TimeStrToMs(const ATimeStr: UnicodeString): UInt64;

    public
      constructor Create(); override;

      procedure SetProperty(const AMember: TJSONMember); override;
      class procedure GetSupportedFields(const AList: TStrings); override;

      function MsToDateTime(const AMs: UInt64): TDateTime;

    published
      property State: TKodiPlayerState read fState write fState default psUnknown;
      property ChannelID: Int64 read fChannelID write fChannelID default 0;
      property Name: UnicodeString read fName write fName;
      property StartTime: UInt64 read fStartTime write fStartTime default 0; // ms (since start of calendar, see TTimestmap)
      property EndTime: UInt64 read fEndTime write fEndTime default 0; // ms (since start of calendar, see TTimestmap)
      property Position: LongWord read fPosition write fPosition default 0; // ms
      property Length: LongWord read fLength write fLength default 0; // ms
      property Genre: UnicodeString read fGenre write fGenre;
  end;

  TKodiPVR = class(TObject)
    private
      fProperties: TKodiPVRProperties;
      fChannelList: TKodiPVRChannelList;
      fCurrentProgram: TKodiPVRCurrentProgram;

    public
      constructor Create();
      destructor Destroy(); override;

      property Properties: TKodiPVRProperties read fProperties;
      property ChannelList: TKodiPVRChannelList read fChannelList;
      property CurrentProgram: TKodiPVRCurrentProgram read fCurrentProgram;
  end;

  TKodiBasePlayerQuery = class(TObject)
    strict private
      fPlayer: TKodiBasePlayer;

    public
      constructor Create(const APlayer: TKodiBasePlayer);

      procedure Execute(const AKodiClient: TJSONRPCClient;
        const AKodiRequest: TJSONRPCRequest; var AActivePlayer: TKodiPlayerKind); virtual;
  end;

  TKodiPlayerWithPlaylistQuery = class(TKodiBasePlayerQuery)
    strict private
      fPlayer: TKodiPlayerWithPlaylist;

      procedure UpdatePlaylistItem(const APlayer: TKodiPlayerWithPlaylist;
        const AItemIndex: Integer; const AItem: TObject);

    protected
      function UpdatePlayList(const AKodiClient: TJSONRPCClient; const AKodiRequest: TJSONRPCRequest): Boolean; virtual;

    public
      constructor Create(const APlayer: TKodiPlayerWithPlaylist);

      procedure Execute(const AKodiClient: TJSONRPCClient;
        const AKodiRequest: TJSONRPCRequest; var AActivePlayer: TKodiPlayerKind); override;
  end;

  TKodiVideoPlayerQuery = class(TKodiPlayerWithPlaylistQuery)
    strict private
      fPVR: TKodiPVR;

    private
      fPlayer: TKodiVideoPlayer;

    protected
      function UpdatePlayList(const AKodiClient: TJSONRPCClient; const AKodiRequest: TJSONRPCRequest): Boolean; override;

    public
      constructor Create(const APlayer: TKodiVideoPlayer; const APVR: TKodiPVR);
  end;

  TKodiQueryThread = class(TThread)
    strict private
      fKodiClient: TJSONRPCClient;
      fKodiRequest: TJSONRPCRequest;

      fConnectEvent: TEvent;
      fQueryPauseEvent: TEvent;
      fCancelSendRequestEvent: TEvent;

      fQueryInterval: LongWord;

      fConnection: TKodiConnection;
      fActivePlayer: TKodiPlayerKind;
      fAudioPlayer: TKodiAudioPlayer;
      fAudioPlayerQuery: TKodiPlayerWithPlaylistQuery;
      fVideoPlayer: TKodiVideoPlayer;
      fVideoPlayerQuery: TKodiVideoPlayerQuery;
      fPicturePlayer: TKodiPicturePlayer;
      fPicturePlayerQuery: TKodiBasePlayerQuery;
      fGUI: TKodiGUI;
      fPVR: TKodiPVR;
      fTVShows: TKodiTVShowList;
      fApplication: TKodiApplication;

      function QueryProperties(const ARequestMethod: UnicodeString;
        const AParamsMembers: TJSONMemberList; const AProperties: TKodiGetPropertiesResult): Boolean;

      procedure UpdateApplication(const AApplication: TKodiApplication);
      procedure UpdateGUI(const AGUI: TKodiGUI);
      procedure UpdatePVR(const APVR: TKodiPVR);
      procedure UpdateTVShows(const ATVShows: TKodiTVShowList; const AVideoPlaylist: TKodiVideoPlayerPlaylist);

      procedure ExecuteQuery();

      procedure UpdateData(const AQueryThread: TKodiQueryThread;
                           const AAudioPlayerQuery: TKodiBasePlayerQuery;
                           const AVideoPlayerQuery: TKodiVideoPlayerQuery;
                           const APicturePlayerQuery: TKodiBasePlayerQuery;
                           const AGUI: TKodiGUI;
                           const APVR: TKodiPVR;
                           const ATVShows: TKodiTVShowList;
                           const AApplication: TKodiApplication);

    protected
      procedure Execute(); override;

    public
      constructor Create();
      destructor Destroy(); override;

      procedure Connect(const AHost: UnicodeString; const APort: Integer);
      procedure Disconnect();
      procedure Reconnect();

      property JSONClient: TJSONRPCClient read fKodiClient;
      property Connection: TKodiConnection read fConnection;
      property ActivePlayer: TKodiPlayerKind read fActivePlayer;
      property AudioPlayer: TKodiAudioPlayer read fAudioPlayer;
      property VideoPlayer: TKodiVideoPlayer read fVideoPlayer;
      property PicturePlayer: TKodiPicturePlayer read fPicturePlayer;
      property GUI: TKodiGUI read fGUI;
      property PVR: TKodiPVR read fPVR;
      property TVShows: TKodiTVShowList read fTVShows;
      property Application: TKodiApplication read fApplication;
  end;

function JSONTimeFormatToMilliseconds(const AJSONTimeObject: TJSONObjectMember): LongWord;

implementation

uses
  Windows,  // for GetTickCount()
  Common, // for TIDMap
  TypInfo; // for GetEnumName()

{$REGION ' Helper functions '}

function JSONTimeFormatToMilliseconds(const AJSONTimeObject: TJSONObjectMember): LongWord;
var
  lFloatMember: TJSONFloatMember;

begin
  result := 0;

  lFloatMember := AJSONTimeObject.Members.Find('hours', TJSONFloatMember) as TJSONFloatMember;
  if Assigned(lFloatMember) then
    // use of ToLongWord() to fix negative value bug, http://forum.kodi.tv/showthread.php?tid=224721
    result := result + lFloatMember.ToLongWord() * 3600000;

  lFloatMember := AJSONTimeObject.Members.Find('minutes', TJSONFloatMember) as TJSONFloatMember;
  if Assigned(lFloatMember) then
    result := result + lFloatMember.ToLongWord() * 60000;

  lFloatMember := AJSONTimeObject.Members.Find('seconds', TJSONFloatMember) as TJSONFloatMember;
  if Assigned(lFloatMember) then
    result := result + lFloatMember.ToLongWord() * 1000;

  lFloatMember := AJSONTimeObject.Members.Find('milliseconds', TJSONFloatMember) as TJSONFloatMember;
  if Assigned(lFloatMember) then
    result := result + lFloatMember.ToLongWord();
end;

function PlayerGetItem(const AKodiClient: TJSONRPCClient; const AKodiRequest: TJSONRPCRequest;
  const APlayerID: Integer; const AProperties: TStrings; out AItem: TJSONObjectMember): Boolean;
var
  lArrayMember: TJSONArrayMember;
  lErrorMessage: UnicodeString;
  lQueryResult: TJSONObjectMember;
  lResponse: TJSONRPCResponse;

begin
  result := false;

  AItem := nil;

  AKodiRequest.Reset();
  AKodiRequest.Method := 'Player.GetItem';
  AKodiRequest.Params.Members.Add('playerid', APlayerID);

  lArrayMember := TJSONArrayMember.Create('properties');
  lArrayMember.Values.Add(AProperties);
  AKodiRequest.Params.Members.Add(lArrayMember);

  lErrorMessage := '';
  if AKodiClient.SendRequest(AKodiRequest, coKodiResponseTimeout, lResponse, lErrorMessage) then begin

    if lResponse.Result is TJSONObjectMember then begin
      lQueryResult := TJSONObjectMember(lResponse.Result);

      AItem := lQueryResult.Members.Find('item', TJSONObjectMember) as TJSONObjectMember;
      if Assigned(AItem) then begin
        AItem := lQueryResult.Members.Extract(AItem) as TJSONObjectMember;

        if Assigned(AItem) then
          result := true;
      end;
    end;

    lResponse.Free;
  end;
end;


{$ENDREGION}

{$REGION ' TKodiPlayableFileItem '}

class procedure TKodiPlayableFileItem.GetSupportedFields(const AList: TStrings);
begin
  AList.Add('title');
  AList.Add('file');
end;

procedure TKodiPlayableFileItem.SetProperty(const AMember: TJSONMember);
var
  lTitle, f, e: UnicodeString;

begin
  if AMember.Name = 'file' then begin
    fFilename := AMember.ToSimpleValue();

    // file is returned before title, using file stem as fallback title *1
    f := ExtractFileName(fFilename);
    e := ExtractFileExt(f);

    fTitle := Copy(f, 1, System.Length(f) - System.Length(e));
  end else
  if AMember.Name = 'title' then begin
    lTitle := AMember.ToSimpleValue();

    // use title only when not empty to avoid overrite fallback (see *1)
    if lTitle <> '' then
      fTitle := lTitle;
  end else
    inherited;
end;

{$ENDREGION}

{$REGION ' TKodiAudioPlayerPlayableItem '}

class procedure TKodiAudioPlayerPlayableItem.GetSupportedFields(const AList: TStrings);
begin
  inherited;

  AList.Add('album');
  AList.Add('artist');
  AList.Add('comment');
  AList.Add('genre');
  AList.Add('duration');
end;

procedure TKodiAudioPlayerPlayableItem.SetProperty(const AMember: TJSONMember);
begin
  // for compatibility with generic kodi queries
  if AMember.Name = 'duration' then
    fLength := StrToIntDef(AMember.ToSimpleValue(), 0) * 1000
  else
    inherited;
end;

{$ENDREGION}

{$REGION ' TKodiAudioPlayerPlaylist '}

function TKodiAudioPlayerPlaylist.GetItem(const AIndex: Integer): TKodiAudioPlayerPlayableItem;
begin
  result := TKodiAudioPlayerPlayableItem(inherited GetItem(AIndex));
end;

procedure TKodiAudioPlayerPlaylist.SetItem(const AIndex: Integer; const AItem: TKodiAudioPlayerPlayableItem);
begin
  inherited SetItem(AIndex, AItem);
end;

function TKodiAudioPlayerPlaylist.GetItemClass(): TKodiPlayableItemClass;
begin
  result := TKodiAudioPlayerPlayableItem;
end;

{$ENDREGION}

{$REGION ' TKodiPlaylist '}

constructor TKodiPlaylist.Create();
begin
  inherited;

  fItemList := TObjectList.Create(true);
end;

destructor TKodiPlaylist.Destroy();
begin
  fItemList.Free;

  inherited;
end;

function TKodiPlaylist.GetCount(): Integer;
begin
  result := fItemList.Count;
end;

procedure TKodiPlaylist.SetCount(const AValue: Integer);
begin
  fItemList.Count := AValue;
end;

function TKodiPlaylist.GetItem(const AIndex: Integer): TKodiPlayableItem;
begin
  if (AIndex >= 0) and (AIndex < fItemList.Count) then
    result := TKodiPlayableItem(fItemList[AIndex])
  else
    result := nil;
end;

procedure TKodiPlaylist.SetItem(const AIndex: Integer; const AItem: TKodiPlayableItem);
begin
  if (AIndex >= 0) and (AIndex < fItemList.Count) then
    fItemList[AIndex] := AItem;
end;

procedure TKodiPlaylist.Reset();
begin
  fItemList.Clear();
end;

function TKodiPlaylist.IsReset(): Boolean;
begin
  result := fItemList.Count = 0;
end;

{$ENDREGION}

{$REGION ' TKodiBasePlayer '}

function TKodiBasePlayer.GetKodiPlayerType(): UnicodeString;
var
  lKind: TKodiPlayerKind;

begin
  lKind := GetKind();

  result := GetEnumName(TypeInfo(TKodiPlayerKind), Ord(lKind));
  result := Copy(result, 3, Length(result));
  result := LowerCase(result);
end;

{$ENDREGION}

{$REGION ' TKodiPlayerWithPlaylist '}

constructor TKodiPlayerWithPlaylist.Create();
var
  lPlayListClass: TKodiPlaylistClass;

begin
  inherited;

  lPlayListClass := GetPlaylistClass();
  if not Assigned(lPlaylistClass) then
    lPlaylistClass := TKodiPlaylist;

  fPlaylist := lPlaylistClass.Create();
end;

destructor TKodiPlayerWithPlaylist.Destroy();
begin
  fPlaylist.Free;

  inherited;
end;

{$ENDREGION}

{$REGION ' TKodiAudioPlayer '}

function TKodiAudioPlayer.GetKind(): TKodiPlayerKind;
begin
  result := pkAudio;
end;

function TKodiAudioPlayer.GetPlaylist(): TKodiAudioPlayerPlaylist;
begin
  result := TKodiAudioPlayerPlaylist(inherited Playlist);
end;

function TKodiAudioPlayer.GetPlaylistClass(): TKodiPlaylistClass;
begin
  result := TKodiAudioPlayerPlaylist;
end;

{$ENDREGION}

{$REGION ' TKodiVideoPlayerPlaylist '}

function TKodiVideoPlayerPlaylist.GetItem(const AIndex: Integer): TKodiVideoPlayerPlayableItem;
begin
  result := TKodiVideoPlayerPlayableItem(inherited GetItem(AIndex));
end;

procedure TKodiVideoPlayerPlaylist.SetItem(const AIndex: Integer; const AItem: TKodiVideoPlayerPlayableItem);
begin
  inherited SetItem(AIndex, AItem);
end;

function TKodiVideoPlayerPlaylist.GetItemClass(): TKodiPlayableItemClass;
begin
  result := TKodiVideoPlayerPlayableItem;
end;

{$ENDREGION}

{$REGION ' TKodiVideoPlayer '}

function TKodiVideoPlayer.GetKind(): TKodiPlayerKind;
begin
  result := pkVideo;
end;

function TKodiVideoPlayer.GetPlaylist(): TKodiVideoPlayerPlaylist;
begin
  result := TKodiVideoPlayerPlaylist(inherited Playlist);
end;

function TKodiVideoPlayer.GetPlaylistClass(): TKodiPlaylistClass;
begin
  result := TKodiVideoPlayerPlaylist;
end;

{$ENDREGION}

{$REGION ' TKodiPicturePlayer '}

function TKodiPicturePlayer.GetKind(): TKodiPlayerKind;
begin
  result := pkPicture;
end;

{$ENDREGION}

{$REGION ' TKodiVideoPlayerPlaylistGenericItem '}

class procedure TKodiVideoPlayerPlayableItem.GetSupportedFields(const AList: TStrings);
begin
  inherited;

  AList.Add('runtime');
  AList.Add('duration');
  AList.Add('genre');
  AList.Add('director');
  AList.Add('tagline');
  AList.Add('plot');
  AList.Add('year');
  AList.Add('writer');
  AList.Add('mpaa');
  AList.Add('tvshowid');
  AList.Add('season');
  AList.Add('episode');
end;

procedure TKodiVideoPlayerPlayableItem.SetProperty(const AMember: TJSONMember);
begin
  // for compatibility with generic kodi queries
  if AMember.Name = 'runtime' then
    fLength := StrToIntDef(AMember.ToSimpleValue(), 0) * 1000
  else
  if AMember.Name = 'episode' then
    fEpisodeNumber := StrToIntDef(AMember.ToSimpleValue(), 0)
  else
    inherited;
end;

{$ENDREGION}

{$REGION ' TKodiConnection '}

function TKodiConnection.ToString: String;
begin
  result := 'Command=' + GetEnumName(TypeInfo(TKodiConnectionCommand), Ord(Command)) + #13#10 +
            'Host=' + Host + #13#10 +
            'Port=' + IntToStr(Port) + #13#10 +
            'State=' + GetEnumName(TypeInfo(TKodiConnectionState), Ord(State));
end;

{$ENDREGION}

{$REGION ' TKodiGUIProperties '}

class procedure TKodiGUIProperties.GetSupportedFields(const AList: TStrings);
begin
  AList.Add('currentwindow');
  AList.Add('currentcontrol');
end;

procedure TKodiGUIProperties.SetProperty(const AMember: TJSONMember);
var
  lCurrentWindow: TJSONObjectMember;
  lFloatMember: TJSONFloatMember;
  lStringMember: TJSONStringMember;

begin
  if (AMember.Name = 'currentwindow') and (AMember is TJSONObjectMember) then begin
    lCurrentWindow := TJSONObjectMember(AMember);

    lFloatMember := lCurrentWindow.Members.Find('id', TJSONFloatMember) as TJSONFloatMember;
    if lFloatMember <> nil then
      CurrentWindowID := Trunc(lFloatMember.Value)
    else
      CurrentWindowID := 0;

    lStringMember := lCurrentWindow.Members.Find('label', TJSONStringMember) as TJSONStringMember;
    if lStringMember <> nil then
      CurrentWindowLabel := lStringMember.Value
    else
      CurrentWindowLabel := '';
  end else
  if (AMember.Name = 'currentcontrol') and (AMember is TJSONObjectMember) then begin
    lCurrentWindow := TJSONObjectMember(AMember);

    lStringMember := lCurrentWindow.Members.Find('label', TJSONStringMember) as TJSONStringMember;
    if lStringMember <> nil then
      CurrentControl := lStringMember.Value
    else
      CurrentControl := '';
  end else
    inherited SetProperty(AMember);
end;

{$ENDREGION}

{$REGION ' TKodiGUI '}

constructor TKodiGUI.Create();
begin
  inherited;

  fProperties := TKodiGUIProperties.Create();
end;

destructor TKodiGUI.Destroy();
begin
  fProperties.Free;

  inherited;
end;

{$ENDREGION}

{$REGION ' TKodiPVRProperties '}

class procedure TKodiPVRProperties.GetSupportedFields(const AList: TStrings);
begin
  AList.Add('available');
  AList.Add('recording');
  AList.Add('scanning');
end;

{$ENDREGION}

{$REGION ' TKodiPVRCurrentProgram '}

constructor TKodiPVRCurrentProgram.Create();
begin
  inherited;

  fTimeStrFormatSettings := DefaultFormatSettings;
  fTimeStrFormatSettings.DateSeparator := '-';
  fTimeStrFormatSettings.ShortDateFormat := 'yyyy-mm-dd';
  fTimeStrFormatSettings.ShortTimeFormat := 'hh:nn:ss';
end;

class procedure TKodiPVRCurrentProgram.GetSupportedFields(const AList: TStrings);
begin
  AList.Add('uniqueid'); // channel id, is returned back as "id"
  AList.Add('title'); // is returned as "label"
  AList.Add('starttime'); // is returned as UTC
  AList.Add('endtime'); // is returned as UTC
  AList.Add('runtime');
  AList.Add('genre');
end;

procedure TKodiPVRCurrentProgram.SetProperty(const AMember: TJSONMember);
var
  lFloatMember: TJSONFloatMember;
  lStringMember: TJSONStringMember;

begin
  if (AMember.Name = 'id') and (AMember is TJSONFloatMember) then begin
    lFloatMember := TJSONFloatMember(AMember);

    fChannelID := lFloatMember.ToInt64();
  end else
  if (AMember.Name = 'title') and (AMember is TJSONStringMember) then begin
    lStringMember := TJSONStringMember(AMember);

    fName := lStringMember.Value;
  end else
  if (AMember.Name = 'runtime') and (AMember is TJSONFloatMember) then begin
    lFloatMember := TJSONFloatMember(AMember);

    fLength := lFloatMember.ToLongWord() * 60 * 1000; // convert from minutes to ms
  end else
  if (AMember.Name = 'starttime') and (AMember is TJSONStringMember) then begin
    lStringMember := TJSONStringMember(AMember);

    fStartTime := TimeStrToMs(lStringMember.Value);
  end else
  if (AMember.Name = 'endtime') and (AMember is TJSONStringMember) then begin
    lStringMember := TJSONStringMember(AMember);

    fEndTime := TimeStrToMs(lStringMember.Value);
  end else
    inherited;
end;

function TKodiPVRCurrentProgram.TimeStrToMs(const ATimeStr: UnicodeString): UInt64;
var
  lDateTime: TDateTime;
  lTimeStamp: TTimeStamp;

begin
  try
    lDateTime := StrToDateTime(ATimeStr, fTimeStrFormatSettings);
  except
    on e: EConvertError do
      lDateTime := 0;
  end;

  lTimeStamp := DateTimeToTimeStamp(lDateTime);

  // each calculation in a separate line to ensure compiler recognizes a UInt64
  // operation otherwise exception would occure
  result := lTimeStamp.Date;
  result := result * 86400000;
  result := result + lTimeStamp.Time;
end;

function TKodiPVRCurrentProgram.MsToDateTime(const AMs: UInt64): TDateTime;
var
  lTimeStamp: TTimeStamp;
  t: UInt64;

begin
  lTimestamp.Date := AMs div 86400000;

  // each calculation in a separate line to ensure compiler recognizes a UInt64
  // operation otherwise exception would occure
  t := lTimestamp.Date;
  t := t * 86400000;

  lTimestamp.Time := AMs - t;

  result := TimeStampToDateTime(lTimestamp);
end;

{$ENDREGION}

{$REGION ' TKodiPVR '}

constructor TKodiPVR.Create();
begin
  inherited;

  fProperties := TKodiPVRProperties.Create();
  fChannelList := TKodiPVRChannelList.Create();
  fCurrentProgram := TKodiPVRCurrentProgram.Create();
end;

destructor TKodiPVR.Destroy();
begin
  fCurrentProgram.Free;
  fChannelList.Free;
  fProperties.Free;

  inherited;
end;

{$ENDREGION}

{$REGION ' TKodiBasePlayerQuery '}

constructor TKodiBasePlayerQuery.Create(const APlayer: TKodiBasePlayer);
begin
  inherited Create();

  fPlayer := APlayer;
end;

procedure TKodiBasePlayerQuery.Execute(const AKodiClient: TJSONRPCClient;
  const AKodiRequest: TJSONRPCRequest; var AActivePlayer: TKodiPlayerKind);
var
  lDeterminedID: Integer;
  lDeterminedState: TKodiPlayerState;
  lDeterminedPlaylistID: Integer;
  lDeterminedCurrentItemPlaylistIndex: Integer;
  lDeterminedPlaybackPosition: LongWord;
  lErrorMessage: UnicodeString;
  p: Integer;
  lReturnedPlayers, lPlayerProperties: TJSONArrayMember;
  lReturnedPlayer: TJSONObject;
  lReturnedPlayerType: TJSONStringMember;
  lReturnedPlayerID: TJSONFloatMember;
  lPlayerType: String;
  lReturnedPlayerProperties: TJSONObjectMember;
  lResponse, lResponse2: TJSONRPCResponse;
  lIntegerPropertyValue: Integer;
  lReturnedPlaybackPosition: TJSONObjectMember;

begin
  // first set all player states to default values
  lDeterminedState := psUnknown;
  lDeterminedID := -1;
  lDeterminedPlaylistID := -1;
  lDeterminedCurrentItemPlaylistIndex := -1;
  lDeterminedPlaybackPosition := 0;

  if AKodiClient.Connected then begin
    AKodiRequest.Reset();
    AKodiRequest.Method := 'Player.GetActivePlayers';

    lErrorMessage := '';
    if AKodiClient.SendRequest(AKodiRequest, coKodiResponseTimeout, lResponse, lErrorMessage) then begin

      if lResponse.Result is TJSONArrayMember then begin
        lReturnedPlayers := TJSONArrayMember(lResponse.Result);

        lDeterminedState := psInactive;

        p := 0;
        while p < lReturnedPlayers.Values.Count do begin

          lReturnedPlayer := lReturnedPlayers.Values[p] as TJSONObject;
          if Assigned(lReturnedPlayer) then begin

            lReturnedPlayerType := lReturnedPlayer.Members.Find('type', TJSONStringMember) as TJSONStringMember;
            if Assigned(lReturnedPlayerType) then begin

              lPlayerType := fPlayer.GetKodiPlayerType();

              if lReturnedPlayerType.Value = lPlayerType then begin

                lReturnedPlayerID := lReturnedPlayer.Members.Find('playerid', TJSONFloatMember) as TJSONFloatMember;
                if Assigned(lReturnedPlayerID) then begin

                  lDeterminedID := Trunc(lReturnedPlayerID.Value);

                  AKodiRequest.Reset();
                  AKodiRequest.Method := 'Player.GetProperties';
                  AKodiRequest.Params.Members.Add('playerid', lDeterminedID);

                  lPlayerProperties := TJSONArrayMember.Create('properties');
                  lPlayerProperties.Values.Add('speed'); // playback speed
                  lPlayerProperties.Values.Add('position'); // position of current item in playlist
                  lPlayerProperties.Values.Add('time'); // playback position
                  lPlayerProperties.Values.Add('playlistid'); // current playlist id

                  AKodiRequest.Params.Members.Add(lPlayerProperties);

                  if AKodiClient.SendRequest(AKodiRequest, coKodiResponseTimeout, lResponse2, lErrorMessage) then begin

                    if lResponse2.Result is TJSONObjectMember then begin
                      lReturnedPlayerProperties := TJSONObjectMember(lResponse2.Result);

                      if  lReturnedPlayerProperties.GetSimpleValue('playlistid', lIntegerPropertyValue) then
                        lDeterminedPlaylistID := lIntegerPropertyValue;

                      if lReturnedPlayerProperties.GetSimpleValue('speed', lIntegerPropertyValue) then begin
                        if lIntegerPropertyValue = 0 then
                          lDeterminedState := psPaused
                        else
                          lDeterminedState := psPlaying;
                      end;

                      if  lReturnedPlayerProperties.GetSimpleValue('position', lIntegerPropertyValue) then
                        lDeterminedCurrentItemPlaylistIndex := lIntegerPropertyValue;

                      lReturnedPlaybackPosition := lReturnedPlayerProperties.Members.Find('time', TJSONObjectMember) as TJSONObjectMember;
                      if Assigned(lReturnedPlaybackPosition) then
                        lDeterminedPlaybackPosition := JSONTimeFormatToMilliseconds(lReturnedPlaybackPosition);
                    end;

                    lResponse2.Free;
                  end;

                end;
              end;
            end;
          end;

          p := p + 1;
        end;

      end;

      lResponse.Free;
    end;
  end;

  // write determined states to player
  fPlayer.BeginAccess();
  try
    fPlayer.ID := lDeterminedID;
    fPlayer.State := lDeterminedState;
    fPlayer.PlaylistID := lDeterminedPlaylistID;
    fPlayer.CurrentItemPlaylistIndex := lDeterminedCurrentItemPlaylistIndex;
    fPlayer.PlaybackPosition := lDeterminedPlaybackPosition;
  finally
    fPlayer.EndAccess();
  end;

  if fPlayer.State in [psPaused, psPlaying] then
    AActivePlayer := fPlayer.GetKind();
end;

{$ENDREGION}

{$REGION ' TKodiPlayerWithPlaylistQuery '}

constructor TKodiPlayerWithPlaylistQuery.Create(const APlayer: TKodiPlayerWithPlaylist);
begin
  inherited Create(APlayer);

  fPlayer := APlayer;
end;

procedure TKodiPlayerWithPlaylistQuery.UpdatePlaylistItem(const APlayer: TKodiPlayerWithPlaylist;
  const AItemIndex: Integer; const AItem: TObject);
var
  lPlaylistItem: TKodiPlayableItem;
  lPlayListItemClass: TKodiPlayableItemClass;
  lMember: TJSONMember;
  lMemberList: TJSONMemberList;
  m: Integer;

begin
  lPlaylistItem := APlayer.Playlist[AItemIndex];
  if not Assigned(lPlaylistItem) then begin
    lPlayListItemClass := APlayer.Playlist.GetItemClass();
    if Assigned(lPlayListItemClass) then
      lPlaylistItem := lPlayListItemClass.Create();

    APlayer.Playlist[AItemIndex] := lPlaylistItem;
  end;

  if Assigned(lPlaylistItem) then begin
    lPlaylistItem.Reset();
    lPlaylistItem.Index := AItemIndex;

    if AItem is TJSONObject then
      lMemberList := TJSONObject(AItem).Members
    else
    if AItem is TJSONObjectMember then
      lMemberList := TJSONObjectMember(AItem).Members
    else
      lMemberList := nil;

    if Assigned(lMemberList) then begin
      // update field of item
      m := 0;
      while m < lMemberList.Count do begin
        lMember := lMemberList.Retrieve(m);

        if Assigned(lMember) then
          lPlaylistItem.SetProperty(lMember);

        m := m + 1;
      end;
    end;
  end;
end;

function TKodiPlayerWithPlaylistQuery.UpdatePlayList(const AKodiClient: TJSONRPCClient;
  const AKodiRequest: TJSONRPCRequest): Boolean;
var
  lErrorMessage: UnicodeString;
  lArrayMember: TJSONArrayMember;
  lMember: TJSONMember;
  i: Integer;
  lFieldList: TStrings;
  lQueryResult: TJSONObjectMember;
  lPlayListItemClass: TKodiPlayableItemClass;
  lQueryCurrentPlayedItem: Boolean;
  lResponse: TJSONRPCResponse;

begin
  result := false;

  lQueryCurrentPlayedItem := false;

  // update playlist only when connected and player is active because kodi can have only one player active
  if AKodiClient.Connected and // connected
     (fPlayer.State in [psPlaying, psPaused]) // player is active (because kodi can have only one player active)
  then begin

    lFieldList := TStringList.Create();
    try
      lPlayListItemClass := fPlayer.Playlist.GetItemClass();
      if Assigned(lPlayListItemClass) then
        lPlayListItemClass.GetSupportedFields(lFieldList);

      if lFieldList.Count > 0 then begin

        if fPlayer.CurrentItemPlaylistIndex <> -1 then begin // player plays items from a playlist
          // request playlist
          AKodiRequest.Reset();
          AKodiRequest.Method := 'Playlist.GetItems';
          AKodiRequest.Params.Members.Add('playlistid', fPlayer.PlaylistID);

          lArrayMember := TJSONArrayMember.Create('properties');
          lArrayMember.Values.Add(lFieldList);
          AKodiRequest.Params.Members.Add(lArrayMember);

        //  lObjectMember := TJSONObjectMember.Create('limits');
        //  lObjectMember.Members.Add('start', 8);
        //  lObjectMember.Members.Add('end', 9);
        //
        //  fKodiRequest.Params.Members.Add(lObjectMember);

          lErrorMessage := '';
          if AKodiClient.SendRequest(AKodiRequest, coKodiResponseTimeout, lResponse, lErrorMessage) then begin

            if lResponse.Result is TJSONObjectMember then begin
              lQueryResult := TJSONObjectMember(lResponse.Result);

              lArrayMember := lQueryResult.Members.Find('items', TJSONArrayMember) as TJSONArrayMember;
              if Assigned(lArrayMember) then begin

                // update items
                if lArrayMember.Values.Count > 0 then begin

                  result := true;

                  fPlayer.Playlist.BeginAccess();
                  try
                    fPlayer.Playlist.Count := lArrayMember.Values.Count;

                    i := 0;
                    while i < lArrayMember.Values.Count do begin
                      UpdatePlaylistItem(fPlayer, i, lArrayMember.Values[i]);

                      i := i + 1;
                    end;

                  finally
                    fPlayer.Playlist.EndAccess();
                  end;

                end;
              end else begin
                // this case is sometimes entered when Kodi returns CurrentItemPlaylistIndex = 0 but
                // doesnt actually have a playlist (bug!?),

                // try to query current played item as fallback
                lQueryCurrentPlayedItem := true;
              end;
            end;

            lResponse.Free;
          end;

        end else
          // player is not playing from playlist, so query played item directly
          lQueryCurrentPlayedItem := true;

        if lQueryCurrentPlayedItem then begin
          AKodiRequest.Reset();
          AKodiRequest.Method := 'Player.GetItem';
          AKodiRequest.Params.Members.Add('playerid', fPlayer.ID);

          lArrayMember := TJSONArrayMember.Create('properties');
          lArrayMember.Values.Add(lFieldList);
          AKodiRequest.Params.Members.Add(lArrayMember);

          lErrorMessage := '';

          if AKodiClient.SendRequest(AKodiRequest, coKodiResponseTimeout, lResponse, lErrorMessage) then begin

            if lResponse.Result is TJSONObjectMember then begin
              lQueryResult := TJSONObjectMember(lResponse.Result);

              lMember := lQueryResult.Members.Find('item');
              if Assigned(lMember) then begin

                result := true;

                fPlayer.Playlist.BeginAccess();
                try
                  fPlayer.Playlist.Count := 1;

                  UpdatePlaylistItem(fPlayer, 0, lMember);
                finally
                  fPlayer.Playlist.EndAccess();
                end;

                // set current item index
                if fPlayer.CurrentItemPlaylistIndex <> 0 then begin
                  fPlayer.BeginAccess();
                  try
                    fPlayer.CurrentItemPlaylistIndex := 0;
                  finally
                    fPlayer.EndAccess();
                  end;
                end;
              end;
            end;

            lResponse.Free;
          end;
        end;
      end;

    finally
      lFieldList.Free;
    end;
  end;
end;

procedure TKodiPlayerWithPlaylistQuery.Execute(const AKodiClient: TJSONRPCClient;
  const AKodiRequest: TJSONRPCRequest; var AActivePlayer: TKodiPlayerKind);
begin
  inherited;

  if not UpdatePlayList(AKodiClient, AKodiRequest) then
  begin
    // only claim exclusive access when list is not reset to save resources
    if not fPlayer.Playlist.IsReset() then
    begin
      // player not active, so no playlist loaded
      fPlayer.Playlist.BeginAccess();
      try
        fPlayer.Playlist.Reset();
      finally
        fPlayer.Playlist.EndAccess();
      end;
    end;
  end;
end;

{$ENDREGION}

{$REGION ' TKodiVideoPlayerQuery '}

constructor TKodiVideoPlayerQuery.Create(const APlayer: TKodiVideoPlayer; const APVR: TKodiPVR);
begin
  inherited Create(APlayer);

  fPlayer := APlayer;
  fPVR := APVR;
end;

function TKodiVideoPlayerQuery.UpdatePlayList(const AKodiClient: TJSONRPCClient;
  const AKodiRequest: TJSONRPCRequest): Boolean;
var
  lResetPlayList, lResetCurrentProgram: Boolean;
  lProperties: TStringList;
  lItem: TJSONObjectMember;
  lItemType: TJSONStringMember;
  lMember: TJSONMember;
  i: Integer;

begin
  lResetPlayList := false;
  lResetCurrentProgram := true;

  if (fPlayer.State in [psPaused, psPlaying]) then begin
    // check whether played item is actually a channel, then video player is the PVR actually

    lProperties := TStringList.Create();
    try
      fPVR.CurrentProgram.GetSupportedFields(lProperties);

      if PlayerGetItem(AKodiClient, AKodiRequest, fPlayer.ID, lProperties, lItem) then begin
        lItemType := lItem.Members.Find('type', TJSONStringMember) as TJSONStringMember;
        if Assigned(lItemType) then begin
          if lItemType.Value = 'channel' then begin

            fPVR.CurrentProgram.BeginAccess();
            try
              i := 0;
              while i < lItem.Members.Count do begin
                lMember := lItem.Members.Retrieve(i);

                if Assigned(lMember) then
                  fPVR.CurrentProgram.SetProperty(lMember);

                i := i + 1;
              end;

              fPVR.CurrentProgram.State := fPlayer.State;
              fPVR.CurrentProgram.Position := fPlayer.PlaybackPosition;

              lResetCurrentProgram := false;
              lResetPlayList := true;

            finally
              fPVR.CurrentProgram.EndAccess();
            end;

          end;
        end;

        lItem.Free;
      end;

    finally
      lProperties.Free;
    end;
  end;

  if lResetCurrentProgram then begin
    // only claim exclusive access when list is not reset to save resources
    if not fPVR.CurrentProgram.IsReset() then
    begin
      fPVR.CurrentProgram.BeginAccess();
      try
        fPVR.CurrentProgram.Reset();
      finally
        fPVR.CurrentProgram.EndAccess();
      end;
    end;
  end;

  if lResetPlayList then
    result := false
  else
    result := inherited;
end;

{$ENDREGION}

{$REGION ' TKodiQueryThread '}

constructor TKodiQueryThread.Create();
begin
  fConnectEvent := TEvent.Create(nil, false, false, 'ConnectEvent');
  fQueryPauseEvent := TEvent.Create(nil, false, false, 'QueryPauseEvent');
  fCancelSendRequestEvent := TEvent.Create(nil, false, false, 'CancelSendRequestEvent');

  fKodiClient := TJSONRPCClient.Create(fCancelSendRequestEvent);
  fKodiRequest := TJSONRPCRequest.Create();

  fConnection := TKodiConnection.Create();

  fPVR := TKodiPVR.Create();

  fAudioPlayer := TKodiAudioPlayer.Create();
  fAudioPlayerQuery := TKodiPlayerWithPlaylistQuery.Create(fAudioPlayer);

  fVideoPlayer := TKodiVideoPlayer.Create();
  fVideoPlayerQuery := TKodiVideoPlayerQuery.Create(fVideoPlayer, fPVR);

  fPicturePlayer := TKodiPicturePlayer.Create();
  fPicturePlayerQuery := TKodiBasePlayerQuery.Create(fPicturePlayer);

  fGUI := TKodiGUI.Create();

  fTVShows := TKodiTVShowList.Create();

  fApplication := TKodiApplication.Create();

  fQueryInterval := 1000;

  inherited Create(false);
end;

destructor TKodiQueryThread.Destroy();
begin
  // condition taken from inherited destructor code of delphi
  if ThreadID <> 0 then
  begin
    Terminate();

    fCancelSendRequestEvent.SetEvent();
    fQueryPauseEvent.SetEvent();
    fConnectEvent.SetEvent();

    WaitFor();
  end;

  fApplication.Free;

  fTVShows.Free;

  fGUI.Free;

  fPicturePlayerQuery.Free;
  fPicturePlayer.Free;

  fVideoPlayerQuery.Free;
  fVideoPlayer.Free;

  fAudioPlayerQuery.Free;
  fAudioPlayer.Free;

  fPVR.Free;

  fConnection.Free;

  fKodiRequest.Free;
  fKodiClient.Free;
  fCancelSendRequestEvent.Free;
  fQueryPauseEvent.Free;
  fConnectEvent.Free;

  inherited;
end;

procedure TKodiQueryThread.UpdateData(const AQueryThread: TKodiQueryThread;
                                      const AAudioPlayerQuery: TKodiBasePlayerQuery;
                                      const AVideoPlayerQuery: TKodiVideoPlayerQuery;
                                      const APicturePlayerQuery: TKodiBasePlayerQuery;
                                      const AGUI: TKodiGUI;
                                      const APVR: TKodiPVR;
                                      const ATVShows: TKodiTVShowList;
                                      const AApplication: TKodiApplication); //inline;
begin
  AQueryThread.UpdateApplication(AApplication);
  AQueryThread.UpdateGUI(AGUI);
  AQueryThread.UpdatePVR(APVR);

  AAudioPlayerQuery.Execute(fKodiClient, fKodiRequest, fActivePlayer);

  AVideoPlayerQuery.Execute(fKodiClient, fKodiRequest, fActivePlayer);
  AQueryThread.UpdateTVShows(ATVShows, AVideoPlayerQuery.fPlayer.Playlist);

  APicturePlayerQuery.Execute(fKodiClient, fKodiRequest, fActivePlayer);
end;

procedure TKodiQueryThread.ExecuteQuery();
var
  lConnectionCommand: TKodiConnectionCommand;
  lCurrentTimeStamp, lCurrentQueryInterval, lLastQueryTimeStamp: LongWord;
  lEndQuery: Boolean;

begin
  lEndQuery := false;
  lLastQueryTimeStamp := 0;
  while not Terminated and not lEndQuery do begin

    lCurrentTimeStamp := GetTickCount();
    lCurrentQueryInterval := lCurrentTimeStamp - lLastQueryTimeStamp;
    if lCurrentQueryInterval < fQueryInterval then
      fQueryPauseEvent.WaitFor(fQueryInterval - lCurrentQueryInterval);

    if not Terminated then begin

      fConnection.BeginAccess();
      try
        lConnectionCommand := fConnection.Command;
      finally
        fConnection.EndAccess();
      end;

      if lConnectionCommand = ccNone then begin

        if fKodiClient.Connected then begin
          UpdateData(Self, fAudioPlayerQuery, fVideoPlayerQuery, fPicturePlayerQuery, fGUI, fPVR, fTVShows, fApplication);

          lLastQueryTimeStamp := lCurrentTimeStamp;
        end else begin
          Reconnect();

          lEndQuery := true;
        end;

      end else
        lEndQuery := true;

    end;
  end;
end;

procedure TKodiQueryThread.Execute();
var
  lConnectionCommand: TKodiConnectionCommand;
  lHost: UnicodeString;
  lPort: LongWord;
  lDisconnect: Boolean;

  procedure SetConnectionState(const AConnection: TKodiConnection; const AState: TKodiConnectionState); inline;
  begin
    AConnection.BeginAccess();
    try
      AConnection.State := AState;
    finally
      AConnection.EndAccess();
    end;
  end;

begin
  // inherited;

  while not Terminated do begin

    fConnection.BeginAccess();
    try
      lConnectionCommand := fConnection.Command;

      // reset command so that it is not executed again next time
      fConnection.Command := ccNone;

      lHost := fConnection.Host;
      lPort := fConnection.Port;
    finally
      fConnection.EndAccess();
    end;

    if lConnectionCommand = ccNone then begin

      if fConnection.State = csConnecting then begin
        if fKodiClient.Connected then begin
          SetConnectionState(fConnection, csConnected);

          ExecuteQuery();
        end;
      end else
      if fKodiClient.Connected then
        ExecuteQuery();
    end;

    if lConnectionCommand in [ccConnect, ccReconnect] then begin

      if lConnectionCommand = ccReconnect then
        lDisconnect := true
      else begin
        if fKodiClient.Connected then begin
          // we only need to disconnect when its another host or port we want to connect to
          if (lHost <> fKodiClient.Host) or (lPort <> fKodiClient.Port) then
            lDisconnect := true
          else
            lDisconnect := false;
        end else
          lDisconnect := false;
      end;

      if lDisconnect then begin
        SetConnectionState(fConnection, csDisconnecting);

        fKodiClient.Disconnect();
      end;

      SetConnectionState(fConnection, csConnecting);

      while not Terminated and (fConnection.Command = ccNone) and not fKodiClient.Connected do begin
        // calling connect in loop is not ideal as Connect() itself e.g. initializes socket over and over again,
        // handling via provided events would be the better solution but it works for now, so what the heck...
        fKodiClient.Connect(lHost, lPort);

        fConnectEvent.WaitFor(fQueryInterval);
      end;

      if not Terminated and (fConnection.Command = ccNone) then begin
        if fKodiClient.Connected then begin
          SetConnectionState(fConnection, csConnected);

          ExecuteQuery();
        end else begin
          UpdateData(Self, fAudioPlayerQuery, fVideoPlayerQuery, fPicturePlayerQuery, fGUI, fPVR, fTVShows, fApplication);

          SetConnectionState(fConnection, csErrorOccured);

          fConnectEvent.WaitFor(INFINITE);
        end;
      end;

    end else
    if lConnectionCommand = ccDisconnect then begin

      if fKodiClient.Connected then begin
        SetConnectionState(fConnection, csDisconnecting);

        fKodiClient.Disconnect();
      end;

      UpdateData(Self, fAudioPlayerQuery, fVideoPlayerQuery, fPicturePlayerQuery, fGUI, fPVR, fTVShows, fApplication);

      SetConnectionState(fConnection, csDisconnected);

      fConnectEvent.WaitFor(INFINITE);
    end else
    if fConnection.State = csConnecting then
      ThreadSwitch()
    else
      fConnectEvent.WaitFor(INFINITE);

  end;
end;

function TKodiQueryThread.QueryProperties(const ARequestMethod: UnicodeString;
  const AParamsMembers: TJSONMemberList; const AProperties: TKodiGetPropertiesResult): Boolean;
var
  lQueryResult: TJSONObjectMember;
  lProperties: TJSONArrayMember;
  lErrorMessage: UnicodeString;
  i, l: Integer;
  lSupportedFields: TStrings;
  lParamsMemberList: TJSONMemberList;
  lParamsMember: TJSONMemberListItem;
  lResponse: TJSONRPCResponse;

begin
  result := false;

  if fKodiClient.Connected then begin
    fKodiRequest.Reset();

    fKodiRequest.Method := ARequestMethod;

    if AParamsMembers <> nil then begin
      // create temporary member list to extract members later from fKodiRequest.Params.Members,
      // this is crucial as items are added with static = true which mean they wont be freed until
      // fKodiRequest is freed and fKodiRequest is only released when plugin ends -> list of members
      // would grow with each call to QueryProperties()
      lParamsMemberList := TJSONMemberList.Create(true);

      l := AParamsMembers.Count - 1;
      for i := 0 to l do begin
        lParamsMember := TJSONMemberListItem.Create(AParamsMembers[i].Member, true);

        lParamsMemberList.Add(lParamsMember);

        fKodiRequest.Params.Members.Add(lParamsMember);
      end;
    end
    else
      lParamsMemberList := nil;

    lProperties := TJSONArrayMember.Create('properties');

    lSupportedFields := TStringList.Create();
    try
      AProperties.GetSupportedFields(lSupportedFields);

      lProperties.Values.Add(lSupportedFields);
    finally
      lSupportedFields.Free;
    end;

    fKodiRequest.Params.Members.Add(lProperties);

    lErrorMessage := '';
    if fKodiClient.SendRequest(fKodiRequest, coKodiResponseTimeout, lResponse, lErrorMessage) then begin

      if lResponse.Result is TJSONObjectMember then begin
        lQueryResult := TJSONObjectMember(lResponse.Result);

        AProperties.BeginAccess();
        try
          l := lQueryResult.Members.Count - 1;
          if l >= 0 then begin

            for i := 0 to l do
              AProperties.SetProperty(lQueryResult.Members[i].Member);

            result := true;
          end;

        finally
          AProperties.EndAccess();
        end;
      end;

      lResponse.Free;
    end;

    if lParamsMemberList <> nil then begin
      l := lParamsMemberList.Count - 1;
      for i := 0 to l do
        fKodiRequest.Params.Members.Extract(lParamsMemberList[i]);

      lParamsMemberList.Free;
    end;
  end;

  if not result then begin
    AProperties.BeginAccess();
    try
      if not AProperties.IsReset() then
        AProperties.Reset();

    finally
      AProperties.EndAccess();
    end;
  end;
end;

procedure TKodiQueryThread.UpdateApplication(const AApplication: TKodiApplication);
begin
  // http://kodi.wiki/view/JSON-RPC_API/v6#Application.GetProperties
  QueryProperties('Application.GetProperties', nil, AApplication.Properties);
end;

procedure TKodiQueryThread.UpdateGUI(const AGUI: TKodiGUI);
begin
  // http://kodi.wiki/view/JSON-RPC_API/v6#GUI.GetProperties
  QueryProperties('GUI.GetProperties', nil, AGUI.Properties);
end;

procedure TKodiQueryThread.UpdatePVR(const APVR: TKodiPVR);
var
  lResetChannelList: Boolean;
  lChannelGroupID, lChannelID: Integer;
  lQueryResult: TJSONObjectMember;
  lChannelGroups, lChannels: TJSONArrayMember;
  lFloatMember: TJSONFloatMember;
  lStringMember: TJSONStringMember;
  lValue, lValue2: TJSONValue;
  lChannelGroupJSONObject, lChannelJSONObject: TJSONObject;
  lErrorMessage, lChannelLabel, lChannelGroupLabel: UnicodeString;
  i, l, i2, l2: Integer;
  lResponse, lKodiResponse: TJSONRPCResponse;
  lChannel: TKodiPVRChannel;
  lDeprecatedChannels: TObjectList;

begin
  lResetChannelList := true;
  lKodiResponse := nil;

  // http://kodi.wiki/view/JSON-RPC_API/v6#PVR.GetProperties
  QueryProperties('PVR.GetProperties', nil, APVR.Properties);

  if APVR.Properties.Available then begin
    fKodiRequest.Reset();

    // update PVR channel list
    fKodiRequest.Method := 'PVR.GetChannelGroups';
    fKodiRequest.Params.Members.Add('channeltype', 'tv');

    lErrorMessage := '';
    if fKodiClient.SendRequest(fKodiRequest, coKodiResponseTimeout, lResponse, lErrorMessage) then begin

      if lResponse.Result is TJSONObjectMember then begin
        lQueryResult := TJSONObjectMember(lResponse.Result);

        lChannelGroups := lQueryResult.Members.Find('channelgroups', TJSONArrayMember) as TJSONArrayMember;
        if lChannelGroups <> nil then begin

          APVR.ChannelList.BeginAccess();
          try
            lDeprecatedChannels := TObjectList.Create(false);
            try
              APVR.ChannelList.CopyTo(lDeprecatedChannels);

              l := lChannelGroups.Values.Count - 1;
              for i := 0 to l do begin
                lValue := lChannelGroups.Values[i];

                if lValue is TJSONObject then begin
                  lChannelGroupJSONObject := TJSONObject(lValue);

                  lFloatMember := lChannelGroupJSONObject.Members.Find('channelgroupid', TJSONFloatMember) as TJSONFloatMember;
                  if lFloatMember <> nil then begin
                    lChannelGroupID := Round(lFloatMember.Value);

                    lStringMember := lChannelGroupJSONObject.Members.Find('label', TJSONStringMember) as TJSONStringMember;
                    if lStringMember <> nil then begin
                      lChannelGroupLabel := lStringMember.Value;

                      fKodiRequest.Reset();

                      // update PVR channel list
                      fKodiRequest.Method := 'PVR.GetChannels';
                      fKodiRequest.Params.Members.Add('channelgroupid', lChannelGroupID);

                      lErrorMessage := '';
                      if fKodiClient.SendRequest(fKodiRequest, coKodiResponseTimeout, lKodiResponse, lErrorMessage) then begin

                        if lKodiResponse.Result is TJSONObjectMember then begin
                          lQueryResult := TJSONObjectMember(lKodiResponse.Result);

                          lChannels := lQueryResult.Members.Find('channels', TJSONArrayMember) as TJSONArrayMember;
                          if lChannels <> nil then begin

                            l2 := lChannels.Values.Count - 1;
                            for i2 := 0 to l2 do begin
                              lValue2 := lChannels.Values[i2];

                              if lValue2 is TJSONObject then begin
                                lChannelJSONObject := TJSONObject(lValue2);

                                lFloatMember := lChannelJSONObject.Members.Find('channelid', TJSONFloatMember) as TJSONFloatMember;
                                if lFloatMember <> nil then begin
                                  lChannelID := Round(lFloatMember.Value);

                                  lStringMember := lChannelJSONObject.Members.Find('label', TJSONStringMember) as TJSONStringMember;
                                  if lStringMember <> nil then begin
                                    lChannelLabel := lStringMember.Value;

                                    if APVR.ChannelList.GetChannelByID(lChannelID, lChannel) then
                                      lDeprecatedChannels.Remove(lChannel)
                                    else begin
                                      lChannel := TKodiPVRChannel.Create();

                                      APVR.ChannelList.Add(lChannel);
                                    end;

                                    lChannel.ID := lChannelID;
                                    lChannel.Name := lChannelLabel;
                                  end;
                                end;
                              end;
                            end;
                          end;
                        end;

                        lKodiResponse.Free;
                      end;
                    end;
                  end;
                end;
              end;

            finally
              APVR.ChannelList.Remove(lDeprecatedChannels);

              lDeprecatedChannels.Free;
            end;

            lResetChannelList := false;

          finally
            APVR.ChannelList.EndAccess();
          end;
        end;
      end;

      lResponse.Free;
    end;

    // note:
    //  no update of current program here as current program is updated in video player query
  end;

  if lResetChannelList then
  begin
    // only claim exclusive access when list is not reset to save resources
    if not APVR.ChannelList.IsReset() then
    begin
      // player not active, so no playlist loaded
      APVR.ChannelList.BeginAccess();
      try
        APVR.ChannelList.Reset();
      finally
        APVR.ChannelList.EndAccess();
      end;
    end;
  end;
end;

procedure TKodiQueryThread.UpdateTVShows(const ATVShows: TKodiTVShowList; const AVideoPlaylist: TKodiVideoPlayerPlaylist);
var
  lResetTVShows, lResetTVShow: Boolean;
  i, l, i2, l2, lTVShowID: Integer;
  lDeprecatedTVShows: TObjectList;
  lVideoPlaylistItem: TKodiVideoPlayerPlayableItem;
  lTVShow: TKodiTVShow;
  lTVShowIDMap: TIDMap;
  lSupportedFields: TStrings;
  lProperties: TJSONArrayMember;
  lQueryResult, lTVShowDetails: TJSONObjectMember;
  lErrorMessage: UnicodeString;
  lResponse: TJSONRPCResponse;

begin
  lResetTVShows := true;

  if fKodiClient.Connected then begin

    // get all tv show ids
    lTVShowIDMap := TIDMap.Create();
    try

      AVideoPlaylist.BeginAccess();
      try
        l := AVideoPlaylist.Count - 1;
        if l >= 0 then begin

          for i := 0 to l do begin
            lVideoPlaylistItem := AVideoPlaylist[i];

            if lVideoPlaylistItem.TVShowID > 0 then begin
              if lTVShowIDMap.IndexOf(lVideoPlaylistItem.TVShowID) = -1 then
                lTVShowIDMap.Add(lVideoPlaylistItem.TVShowID, false);
            end;
          end;
        end;

      finally
        AVideoPlaylist.EndAccess();
      end;

      l :=  lTVShowIDMap.Count - 1;
      if l >= 0 then begin

        lDeprecatedTVShows := TObjectList.Create(false);
        lSupportedFields := TStringList.Create();
        ATVShows.BeginAccess();
        try
          ATVShows.CopyTo(lDeprecatedTVShows);

          TKodiTVShow.GetSupportedFields(lSupportedFields);

          for i := 0 to l do begin
            lTVShowID := lTVShowIDMap.Keys[i];

            if ATVShows.GetTVShowByID(lTVShowID, lTVShow) then
              lDeprecatedTVShows.Remove(lTVShow)
            else begin
              lTVShow := TKodiTVShow.Create();

              lTVShow.ID := lTVShowID;

              ATVShows.Add(lTVShow);
            end;

            lResetTVShow := true;

            fKodiRequest.Reset();
            fKodiRequest.Method := 'VideoLibrary.GetTVShowDetails';
            fKodiRequest.Params.Members.Add('tvshowid', lTVShowID);

            lProperties := TJSONArrayMember.Create('properties');
            lProperties.Values.Add(lSupportedFields);
            fKodiRequest.Params.Members.Add(lProperties);

            lErrorMessage := '';
            if fKodiClient.SendRequest(fKodiRequest, coKodiResponseTimeout, lResponse, lErrorMessage) then begin

              if lResponse.Result is TJSONObjectMember then begin
                lQueryResult := TJSONObjectMember(lResponse.Result);

                lTVShowDetails := lQueryResult.Members.Find('tvshowdetails', TJSONObjectMember) as TJSONObjectMember;
                if Assigned(lTVShowDetails) then begin

                  l2 := lTVShowDetails.Members.Count - 1;
                  if l2 >= 0 then begin
                    for i2 := 0 to l2 do
                      lTVShow.SetProperty(lTVShowDetails.Members[i2].Member);

                    lResetTVShow := false;
                  end;
                end;
              end;

              lResponse.Free;
            end;

            if lResetTVShow then
              lTVShow.Reset();
          end;

        finally
          ATVShows.Remove(lDeprecatedTVShows);
          ATVShows.EndAccess();

          lSupportedFields.Free;
          lDeprecatedTVShows.Free;
        end;

        lResetTVShows := false;
      end;

    finally
      lTVShowIDMap.Free;
    end;
  end;

  if lResetTVShows then
  begin
    // only claim exclusive access when list is not reset to save resources
    if not ATVShows.IsReset() then
    begin
      ATVShows.BeginAccess();
      try
        ATVShows.Reset();
      finally
        ATVShows.EndAccess();
      end;
    end;
  end;
end;

procedure TKodiQueryThread.Connect(const AHost: UnicodeString; const APort: Integer);
begin
  fConnection.BeginAccess();
  try
    fConnection.Command := ccConnect;
    fConnection.Host := AHost;
    fConnection.Port := APort;
  finally
    fConnection.EndAccess();
  end;

  fConnectEvent.SetEvent();
end;

procedure TKodiQueryThread.Disconnect();
begin
  fConnection.BeginAccess();
  try
    fConnection.Command := ccDisconnect;
  finally
    fConnection.EndAccess();
  end;

  fConnectEvent.SetEvent();
end;

procedure TKodiQueryThread.Reconnect();
begin
  fConnection.BeginAccess();
  try
    fConnection.Command := ccReconnect;
  finally
    fConnection.EndAccess();
  end;

  fConnectEvent.SetEvent();
end;

{$ENDREGION}

end.

//
// Channel classes
//
// $Id$
//

unit Channel;

interface

uses
  Classes,
  Contnrs,              // for TObjectList
  {$IFDEF FPC}
    gmap,
  {$ELSE}
    Generics.Collections,
  {$ENDIF}
  Common, // for TIntCompare
  SynchronizedObject;

type
  TKodiPVRChannel = class;

  {$IFDEF FPC}
    TKodiPVRChannelIDChannelListItemMap = specialize TMap<Integer, TKodiPVRChannel, TIntCompare>;
  {$ELSE}
    TKodiPVRChannelIDChannelListItemMap = TDictionary<Integer, TKodiPVRChannel>;
  {$ENDIF}

  TKodiPVRChannel = class(TKodiPropertyQueryResult)
    strict private
      fID: Integer;
      fName: UnicodeString;
      fChannelIDChannelListItemMap: TKodiPVRChannelIDChannelListItemMap;

      procedure SetID(const AValue: Integer);

      procedure RegisterToChannelIDChannelListItemMap();
      procedure UnregisterFromChannelIDChannelListItemMap();

    private
      procedure SetChannelIDChannelListItemMap(const AMap: TKodiPVRChannelIDChannelListItemMap);

    public
      destructor Destroy(); override;

    published
      property ID: Integer read fID write SetID;
      property Name: UnicodeString read fName write fName;
  end;

  TKodiPVRChannelListItemList = class(TObjectList)
    strict private
      fChannelIDChannelListItemMap: TKodiPVRChannelIDChannelListItemMap;

    protected
      procedure Notify(APtr: Pointer; AAction: TListNotification); override;

    public
      constructor Create();
      destructor Destroy(); override;

      function GetChannelByID(const AChannelID: Integer; out AChannel: TKodiPVRChannel): Boolean;
  end;

  TKodiPVRChannelList = class(TKodiSynchronizedQueryResult)
    strict private
      function GetCount(): Integer;
      procedure SetCount(const AValue: Integer);

    private
      fItemList: TKodiPVRChannelListItemList;

      function GetItem(const AIndex: Integer): TKodiPVRChannel;
      procedure SetItem(const AIndex: Integer; const AItem: TKodiPVRChannel);

    public
      constructor Create(); override;
      destructor Destroy(); override;

      procedure Reset();
      function IsReset(): Boolean;

      procedure Add(const AItem: TKodiPVRChannel);
      procedure Remove(const AItems: TObjectList);
      procedure CopyTo(const AObjectList: TObjectList);
      function GetChannelByID(const AChannelID: Integer; out AChannel: TKodiPVRChannel): Boolean;

      property Count: Integer read GetCount write SetCount;
      property Items[const AIndex: Integer]: TKodiPVRChannel read GetItem write SetItem; default;
  end;

implementation

uses
  SysUtils; // for FreeAndNil()

{$REGION ' TKodiPVRChannel '}

destructor TKodiPVRChannel.Destroy();
begin
  UnregisterFromChannelIDChannelListItemMap();

  inherited;
end;

procedure TKodiPVRChannel.RegisterToChannelIDChannelListItemMap();
{$IFDEF FPC}
var
  i: TKodiPVRChannelIDChannelListItemMap.TIterator;
{$ENDIF}

begin
  if fChannelIDChannelListItemMap <> nil then begin
    i := fChannelIDChannelListItemMap.Find(fID);
    if i <> nil then begin
      i.Value := self;

      i.Free;
    end else
      fChannelIDChannelListItemMap.Insert(fID, self);
  end;
end;

procedure TKodiPVRChannel.UnregisterFromChannelIDChannelListItemMap();
{$IFDEF FPC}
var
  i: TKodiPVRChannelIDChannelListItemMap.TIterator;
{$ENDIF}

begin
  if fChannelIDChannelListItemMap <> nil then begin
    i := fChannelIDChannelListItemMap.Find(fID);
    if i <> nil then begin
      fChannelIDChannelListItemMap.Delete(i.Key);

      i.Free;
    end;
  end;
end;

procedure TKodiPVRChannel.SetID(const AValue: Integer);
begin
  if AValue <> fID then begin
    UnregisterFromChannelIDChannelListItemMap();

    fID := AValue;

    RegisterToChannelIDChannelListItemMap();
  end;
end;

procedure TKodiPVRChannel.SetChannelIDChannelListItemMap(const AMap: TKodiPVRChannelIDChannelListItemMap);
begin
  UnregisterFromChannelIDChannelListItemMap();

  fChannelIDChannelListItemMap := AMap;

  RegisterToChannelIDChannelListItemMap();
end;

{$ENDREGION}

{$REGION ' TKodiPVRChannelListItemList '}

constructor TKodiPVRChannelListItemList.Create();
begin
  inherited Create(true);

  fChannelIDChannelListItemMap := TKodiPVRChannelIDChannelListItemMap.Create();
end;

destructor TKodiPVRChannelListItemList.Destroy();
var
  i, l: Integer;

begin
  l := Count - 1;
  for i := 0 to l do
    TKodiPVRChannel(GetItem(i)).SetChannelIDChannelListItemMap(nil);

  FreeAndNil(fChannelIDChannelListItemMap);

  inherited;
end;

procedure TKodiPVRChannelListItemList.Notify(APtr: Pointer; AAction: TListNotification);
begin
  if AAction in [lnDeleted, lnExtracted] then begin
    if TObject(APtr) is TKodiPVRChannel then
      TKodiPVRChannel(APtr).SetChannelIDChannelListItemMap(nil);
  end;

  inherited Notify(APtr, AAction);

  if AAction = lnAdded then begin
    if TObject(APtr) is TKodiPVRChannel then
      TKodiPVRChannel(APtr).SetChannelIDChannelListItemMap(fChannelIDChannelListItemMap);
  end;
end;

function TKodiPVRChannelListItemList.GetChannelByID(const AChannelID: Integer;
  out AChannel: TKodiPVRChannel): Boolean;
{$IFDEF FPC}
var
  i: TKodiPVRChannelIDChannelListItemMap.TIterator;
{$ENDIF}

begin
  result := false;

  if fChannelIDChannelListItemMap <> nil then begin
    i := fChannelIDChannelListItemMap.Find(AChannelID);
    if i <> nil then begin
      AChannel := i.Value;

      i.Free;

      result := AChannel <> nil;
    end;
  end;
end;

{$ENDREGION}

{$REGION ' TKodiPVRChannelList '}

constructor TKodiPVRChannelList.Create();
begin
  inherited;

  fItemList := TKodiPVRChannelListItemList.Create();
end;

destructor TKodiPVRChannelList.Destroy();
begin
  Self.BeginAccess();
  try
    fItemList.Free;
  finally
    Self.EndAccess();
  end;

  inherited;
end;

procedure TKodiPVRChannelList.Add(const AItem: TKodiPVRChannel);
begin
  fItemList.Add(AItem);
end;

procedure TKodiPVRChannelList.Remove(const AItems: TObjectList);
var
  i, l: Integer;

begin
  l := AItems.Count - 1;
  for i := 0 to l do
    fItemList.Remove(AItems[i]);
end;

procedure TKodiPVRChannelList.CopyTo(const AObjectList: TObjectList);
begin
  AObjectList.Assign(fItemList, laCopy);
end;

function TKodiPVRChannelList.GetChannelByID(const AChannelID: Integer; out AChannel: TKodiPVRChannel): Boolean;
begin
  if Assigned(fItemList) then
    result := fItemList.GetChannelByID(AChannelID, AChannel)
  else
    result := false;
end;

function TKodiPVRChannelList.GetCount(): Integer;
begin
  if Assigned(fItemList) then
    result := fItemList.Count
  else
    result := 0;
end;

procedure TKodiPVRChannelList.SetCount(const AValue: Integer);
begin
  if Assigned(fItemList) then
    fItemList.Count := AValue;
end;

function TKodiPVRChannelList.GetItem(const AIndex: Integer): TKodiPVRChannel;
begin
  if (AIndex >= 0) and (AIndex < fItemList.Count) and Assigned(fItemList) then
    result := TKodiPVRChannel(fItemList[AIndex])
  else
    result := nil;
end;

procedure TKodiPVRChannelList.SetItem(const AIndex: Integer; const AItem: TKodiPVRChannel);
begin
  if (AIndex >= 0) and (AIndex < fItemList.Count) and Assigned(fItemList) then
    fItemList[AIndex] := AItem;
end;

procedure TKodiPVRChannelList.Reset();
begin
  if Assigned(fItemList) then
    fItemList.Clear();
end;

function TKodiPVRChannelList.IsReset(): Boolean;
begin
  if Assigned(fItemList) then
    result := fItemList.Count = 0
  else
    result := true;
end;

{$ENDREGION}

end.

